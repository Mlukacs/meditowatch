//
//  MeditoApp.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 30/11/2020.
//

import SwiftUI
import AVFoundation

@main
struct MeditoApp: App {
    
    init() {
        setUpAudio() 
    }
    
    @SceneBuilder var body: some Scene {
        WindowGroup {
                MainTabView()
        }

//        WKNotificationScene(controller: NotificationController.self, category: NotificationConfig.notificationCategory)
    }
    
    private func setUpAudio() {
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback,
                                                            mode: .default,
                                                            policy: .longFormAudio,
                                                            options: [])
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print(error)
        }
    }
}
