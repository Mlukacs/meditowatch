//
//  ComplicationController.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 30/11/2020.
//

import ClockKit
import UIKit

class ComplicationController: NSObject, CLKComplicationDataSource {
    private let icon = UIImage(named: "Icon-24")!
    private let meditoTitle = "Medito"
    private let defaultColor = UIColor(red: 239 / 255, green: 84 / 255, blue: 102 / 255, alpha: 1)
    
    // MARK: - Timeline Configuration
    
    func getSupportedTimeTravelDirections(for complication: CLKComplication,
                                          withHandler handler: @escaping (CLKComplicationTimeTravelDirections) -> Void) {
        handler([])
    }
    
    func getTimelineStartDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        handler(nil)
    }
    
    func getTimelineEndDate(for complication: CLKComplication, withHandler handler: @escaping (Date?) -> Void) {
        handler(nil)
    }
    
    func getPrivacyBehavior(for complication: CLKComplication,
                            withHandler handler: @escaping (CLKComplicationPrivacyBehavior) -> Void) {
        handler(.showOnLockScreen)
    }
    
    // MARK: - Timeline Population
    
    func getCurrentTimelineEntry(for complication: CLKComplication,
                                 withHandler handler: @escaping (CLKComplicationTimelineEntry?) -> Void) {
        // Call the handler with the current timeline entry
        handler(createTimelineEntry(forComplication: complication, date: Date()))
    }
    
    func getTimelineEntries(for complication: CLKComplication,
                            before date: Date,
                            limit: Int,
                            withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        // Call the handler with the timeline entries prior to the given date
        handler(nil)
    }
    
    func getTimelineEntries(for complication: CLKComplication,
                            after date: Date,
                            limit: Int,
                            withHandler handler: @escaping ([CLKComplicationTimelineEntry]?) -> Void) {
        // Call the handler with the timeline entries after to the given date
        handler(nil)
    }
    
    // MARK: - Placeholder Templates
    
    func getLocalizableSampleTemplate(for complication: CLKComplication,
                                      withHandler handler: @escaping (CLKComplicationTemplate?) -> Void) {
        // Use a date more than 24-hours from now--so the complication always shows
        // zero cups and zero mg caffeine.
        let future = Date().addingTimeInterval(25.0 * 60.0 * 60.0)
        let template = createTemplate(forComplication: complication, date: future)
        handler(template)
    }
    
    func getComplicationDescriptors(handler: @escaping ([CLKComplicationDescriptor]) -> Void) {
         let descriptors = [
             CLKComplicationDescriptor(identifier: "complication", displayName: "Medito", supportedFamilies: CLKComplicationFamily.allCases)
             // Multiple complication support can be added here with more descriptors
         ]
         
         // Call the handler with the currently supported complication descriptors
         handler(descriptors)
     }
}

// MARK: - Utils
private extension ComplicationController {
    // MARK: - Private Methods
    
    // Return a timeline entry for the specified complication and date.
    func createTimelineEntry(forComplication complication: CLKComplication, date: Date) -> CLKComplicationTimelineEntry {
        
        // Get the correct template based on the complication.
        let template = createTemplate(forComplication: complication, date: date)
        
        // Use the template and date to create a timeline entry.
        return CLKComplicationTimelineEntry(date: date, complicationTemplate: template)
    }
    
    // Select the correct template based on the complication's family.
    func createTemplate(forComplication complication: CLKComplication, date: Date) -> CLKComplicationTemplate {
        switch complication.family {
        case .modularSmall:
            return createModularSmallTemplate(forDate: date)
        case .modularLarge:
            return createModularLargeTemplate(forDate: date)
        case .utilitarianSmall, .utilitarianSmallFlat:
            return createUtilitarianSmallFlatTemplate(forDate: date)
        case .utilitarianLarge:
            return createUtilitarianLargeTemplate(forDate: date)
        case .circularSmall:
            return createCircularSmallTemplate(forDate: date)
        case .extraLarge:
            return createExtraLargeTemplate(forDate: date)
        case .graphicCorner:
            return createGraphicCornerTemplate(forDate: date)
        case .graphicCircular:
            return createGraphicCircleTemplate(forDate: date)
        case .graphicRectangular:
            return createGraphicRectangularTemplate(forDate: date)
        case .graphicBezel:
            return createGraphicBezelTemplate(forDate: date)
        case .graphicExtraLarge:
            return createGraphicExtraLargeTemplate(forDate: date)
        @unknown default:
            fatalError("*** Unknown Complication Family ***")
        }
    }
}

// MARK: - Template creators
private extension ComplicationController {
    // Return a modular small template.
    func createModularSmallTemplate(forDate date: Date) -> CLKComplicationTemplate {
        // Create the data providers.
        let iconProvider = CLKImageProvider(onePieceImage: icon)
        
        // Create the template using the providers.
        let template = CLKComplicationTemplateModularSmallSimpleImage(imageProvider: iconProvider)
        return template
    }
    
    // Return a modular large template.
    func createModularLargeTemplate(forDate date: Date) -> CLKComplicationTemplate {
        // Create the data providers.
        let titleTextProvider = CLKSimpleTextProvider(text: meditoTitle)
        titleTextProvider.tintColor = defaultColor
        let textProvider = CLKSimpleTextProvider(text: "Enjoy your meditation")
        
        // Create the template using the providers.
        let imageProvider = CLKImageProvider(onePieceImage: icon)
        let template =
            CLKComplicationTemplateModularLargeStandardBody(headerImageProvider: imageProvider,
                                                            headerTextProvider: titleTextProvider,
                                                            body1TextProvider: textProvider)
        return template
    }
    
    // Return a utilitarian small flat template.
    func createUtilitarianSmallFlatTemplate(forDate date: Date) -> CLKComplicationTemplate {
        // Create the data providers.
        let flatUtilitarianImageProvider = CLKImageProvider(onePieceImage: icon)
        let titleTextProvider = CLKSimpleTextProvider(text: meditoTitle)
        // Create the template using the providers.
        let template = CLKComplicationTemplateUtilitarianSmallFlat(textProvider: titleTextProvider,
                                                                   imageProvider: flatUtilitarianImageProvider)
        return template
    }
    
    // Return a utilitarian large template.
    func createUtilitarianLargeTemplate(forDate date: Date) -> CLKComplicationTemplate {
        // Create the data providers.
        let flatUtilitarianImageProvider = CLKImageProvider(onePieceImage: icon)
        let titleTextProvider = CLKSimpleTextProvider(text: meditoTitle)
        // Create the template using the providers.
        let template = CLKComplicationTemplateUtilitarianLargeFlat(textProvider: titleTextProvider,
                                                                   imageProvider: flatUtilitarianImageProvider)
        return template
    }
    
    // Return a circular small template.
    func createCircularSmallTemplate(forDate date: Date) -> CLKComplicationTemplate {
        // Create the data providers.
        let imageProvider = CLKImageProvider(onePieceImage: icon)
        
        // Create the template using the providers.
        let template = CLKComplicationTemplateCircularSmallSimpleImage(imageProvider: imageProvider)
        return template
    }
    
    // Return an extra large template.
    func createExtraLargeTemplate(forDate date: Date) -> CLKComplicationTemplate {
        // Create the data providers.
        let imageProvider = CLKImageProvider(onePieceImage: icon)
        let titleTextProvider = CLKSimpleTextProvider(text: meditoTitle)
        
        // Create the template using the providers.
        let template = CLKComplicationTemplateExtraLargeStackImage(line1ImageProvider: imageProvider,
                                                                   line2TextProvider: titleTextProvider)
        return template
    }
    
    // Return a graphic template that fills the corner of the watch face.
    func createGraphicCornerTemplate(forDate date: Date) -> CLKComplicationTemplate {
        // Create the data providers.
        let imageProvider = CLKFullColorImageProvider(fullColorImage: icon)
        let titleTextProvider = CLKSimpleTextProvider(text: meditoTitle)
        
        // Create the template using the providers.
        let template = CLKComplicationTemplateGraphicCornerTextImage(textProvider: titleTextProvider,
                                                                     imageProvider: imageProvider)

        return template
    }
    
    // Return a graphic circle template.
    func createGraphicCircleTemplate(forDate date: Date) -> CLKComplicationTemplate {
        // Create the data providers.
        let imageProvider = CLKFullColorImageProvider(fullColorImage: icon)
        let titleTextProvider = CLKSimpleTextProvider(text: meditoTitle)
        
        // Create the template using the providers.
        let template =
            CLKComplicationTemplateGraphicCircularStackImage(line1ImageProvider: imageProvider,
                                                             line2TextProvider: titleTextProvider)
        return template
    }
    
    // Return a large rectangular graphic template.
    func createGraphicRectangularTemplate(forDate date: Date) -> CLKComplicationTemplate {
        // Create the data providers.
        let imageProvider = CLKFullColorImageProvider(fullColorImage: icon)
        let titleTextProvider = CLKSimpleTextProvider(text: meditoTitle)
        titleTextProvider.tintColor = defaultColor
        let textProvider = CLKSimpleTextProvider(text: "Enjoy your meditation")
        
        // Create the template using the providers.
        let template =
            CLKComplicationTemplateGraphicRectangularStandardBody(headerImageProvider: imageProvider,
                                                                  headerTextProvider: titleTextProvider,
                                                                  body1TextProvider: textProvider)
        return template
    }
    
    // Return a circular template with text that wraps around the top of the watch's bezel.
    func createGraphicBezelTemplate(forDate date: Date) -> CLKComplicationTemplate {
        
        // Create a graphic circular template with an image provider.
        let circle = CLKComplicationTemplateGraphicCircularImage(imageProvider: CLKFullColorImageProvider(fullColorImage: icon))
        let titleTextProvider = CLKSimpleTextProvider(text: meditoTitle)
        
        // Create the bezel template using the circle template and the text provider.
        let template =
            CLKComplicationTemplateGraphicBezelCircularText(circularTemplate: circle,
                                                            textProvider: titleTextProvider)
        return template
    }
    
    // Returns an extra large graphic template
    func createGraphicExtraLargeTemplate(forDate date: Date) -> CLKComplicationTemplate {
        let imageProvider = CLKFullColorImageProvider(fullColorImage: icon)
        let titleTextProvider = CLKSimpleTextProvider(text: meditoTitle)
        
        return CLKComplicationTemplateGraphicExtraLargeCircularStackImage(line1ImageProvider: imageProvider,
                                                                          line2TextProvider: titleTextProvider)
    }
}

extension UIImage {
  func resized(to newSize: CGSize) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
    defer { UIGraphicsEndImageContext() }

    draw(in: CGRect(origin: .zero, size: newSize))
    return UIGraphicsGetImageFromCurrentImageContext()
  }
}
