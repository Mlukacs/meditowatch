//
//  Double+Extensions.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 08/12/2020.
//

import Foundation

extension Double {
    func timeDisplay() -> String {
        if self.isNaN {
            return "00:00"
        }
        let sec = Int(self.truncatingRemainder(dividingBy: 60))
        let min = Int(self.truncatingRemainder(dividingBy: 3600) / 60)
        return String(format: "%02d:%02d", min, sec)
    }
    /// Rounds the double to decimal places value
    func roundToPlaces(_ places: Int) -> Double {
        guard self != 0, !self.isNaN else {
            return 0
        }
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    func cutOffDecimalsAfter(_ places: Int) -> Double {
        guard self != 0 else {
            return 0
        }
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded(.towardZero) / divisor
    }
}
