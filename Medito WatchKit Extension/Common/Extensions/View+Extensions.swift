//
//  View+Extensions.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 01/12/2020.
//

import SwiftUI

extension View {

    /// Whether the view should be empty.
    /// - Parameter bool: Set to `true` to hide the view (return EmptyView instead). Set to `false` to show the view.
    func isLoading(_ isLoading: Bool) -> some View {
        modifier(LoadingModifier(isLoading: isLoading))
    }
    
    /// Whether the view should be empty.
    /// - Parameter bool: Set to `true` to hide the view (return EmptyView instead). Set to `false` to show the view.
    func hasPlayingContent(_ hasContentLoaded: Bool, view: AnyView) -> some View {
        modifier(PlayingContentModifier(hasContentLoaded: hasContentLoaded, view: view))
    }
    
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape(RoundedCorner(radius: radius, corners: corners) )
    }
}

private struct RoundedCorner: Shape {

    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners

    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}

// MARK: - View modifier

private struct LoadingModifier: ViewModifier {

    let isLoading: Bool

    func body(content: Content) -> some View {
        Group {
            if isLoading {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .white))
            } else {
                content
            }
        }
    }
}

struct PlayingContentModifier: ViewModifier {
    let hasContentLoaded: Bool
    var view: AnyView

    func body(content: Content) -> some View {
        Group {
            if hasContentLoaded {
                content.overlay(PlayButton(view: view),
                                alignment: .bottomTrailing)
                    .edgesIgnoringSafeArea(.bottom)
            } else {
                content
            }
        }
    }
}

private struct PlayButton: View {
    let view: AnyView
    
    var body: some View {
        NavigationLink(destination: LazyView(view)) {
            HStack {
                Spacer()
                ZStack(alignment: .center) {
                    Circle()
                        .foregroundColor(.darkBGColor)
                        .frame(width: 35, height: 35)
                    
                    Image(systemName: "playpause.fill")
                        .resizable()
                        .renderingMode(.template)
                        .scaledToFit()
                        .foregroundColor(.defaultMainCoral)
                        .frame(width: 15, height: 15)
                }
            }
        }.buttonStyle(PlainButtonStyle())
        .padding()
    }
}
