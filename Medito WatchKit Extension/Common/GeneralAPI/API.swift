//
//  Api.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 30/11/2020.
//

import Foundation

final class API: NetworkingService {

    internal var network = NetworkingClient(baseURL: APIConfig.baseUrl)
    private var apiKey: String {
            // 1
            guard let filePath = Bundle.main.path(forResource: "Medito-Info", ofType: "plist") else {
                fatalError("Couldn't find file 'Medito-Info.plist'.")
            }
            // 2
            let plist = NSDictionary(contentsOfFile: filePath)
            guard let value = plist?.object(forKey: "API_KEY") as? String else {
                fatalError("Couldn't find key 'API_KEY' in 'Medito-Info.plist'.")
            }
            // 3
            if value.starts(with: "_") {
                fatalError("Register for a Medito developer account and get an API key at https://meditofoundation.org/volunteer.")
            }
            return value
    }
    
    init() {
        setUp()
    }
}

private extension API {
    func setUp() {
        network.headers["Authorization"] = apiKey
       
        #if DEBUG
        network.logLevels = .debug
        network.timeout = 5
        #else
        network.logLevels = .off
        network.timeout = APIConfig.timeout
        #endif
    }
}
