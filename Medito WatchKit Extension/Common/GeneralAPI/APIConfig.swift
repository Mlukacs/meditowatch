//
//  APIConfig.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 30/11/2020.
//

import Foundation

enum APIConfig {
    static let baseUrl = "https://live.medito.app/api"
    static let homeUrl = "/pages/packs"
    static let dailyURL = "/pages/dailies+today"
    static let timerURL = "/pages/sessions+155"
    static let soundsURL = "/pages/folders+sounds"
    static let sleepURL = "/pages/folders+sleep"
    static let timeout: Double = 15
}
