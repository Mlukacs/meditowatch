//
//  AudioFileAPI.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 02/12/2020.
//

import Combine

protocol AudioFileAPIContract {
    func fetch(contentPath path: String) -> AnyPublisher<AudioDataContainer, Error>
}

extension API: AudioFileAPIContract {
    
    // Read
    func fetch(contentPath path: String) -> AnyPublisher<AudioDataContainer, Error> {
        get(path)
    }
}
