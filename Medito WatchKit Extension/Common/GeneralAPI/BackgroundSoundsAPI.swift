//
//  BackgroundSoundsAPI.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 05/12/2020.
//

import Combine

protocol BackgroundSoundsAPIContract {
    func backgroundSounds() -> AnyPublisher<BackgroundSoundsContainer, Error>
}

extension API: BackgroundSoundsAPIContract {

    func backgroundSounds() -> AnyPublisher<BackgroundSoundsContainer, Error> {
        let path = "/pages/backgroundsounds/files"
        return get(path)
    }
}
