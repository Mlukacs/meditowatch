//
//  PacksAPI.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 30/01/2021.
//

import Combine

protocol PackAPIContract {
    func fetchPack(for path: String) -> AnyPublisher<MindfullPack, Error>
    func packs(for path: String) -> AnyPublisher<MindfullPacksContainer, Error>
}

extension API: PackAPIContract {
    
    // Read
    func fetchPack(for path: String) -> AnyPublisher<MindfullPack, Error> {
        get(path)
    }
    
    // List
    func packs(for path: String) -> AnyPublisher<MindfullPacksContainer, Error> {
        get(path)
    }
}
