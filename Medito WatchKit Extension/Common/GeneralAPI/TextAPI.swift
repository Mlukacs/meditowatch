//
//  TextAPI.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 12/12/2020.
//

import Combine

protocol TextAPIContract {
    func fetch(contentPath path: String) -> AnyPublisher<TextDataContainer, Error>
}

extension API: TextAPIContract {
    
    // Read
    func fetch(contentPath path: String) -> AnyPublisher<TextDataContainer, Error> {
        get(path)
    }
}
