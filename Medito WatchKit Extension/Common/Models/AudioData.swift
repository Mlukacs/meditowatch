//
//  AudioData.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 02/12/2020.
//

import Foundation
import RealmSwift

// MARK: - Audio
struct AudioDataContainer: Codable {
    let data: AudioData
}
extension AudioDataContainer: NetworkingJSONDecodable {}

// MARK: - DataClass
struct AudioData: Codable, Identifiable, Equatable {
    let content: AudioContent
    let id: String
    let title: String
    var isLocal: Bool = false
    
    enum CodingKeys: String, CodingKey {
        case content
        case id
        case title
      }
    
    init(data: Data) throws {
        self = try watchJSONDecoder().decode(AudioData.self, from: data)
    }
    
    init(content: AudioContent, id: String, title: String, isLocal: Bool) {
        self.content = content
        self.id = id
        self.title = title
        self.isLocal = isLocal
    }
    
    func jsonData() -> Data {
        do {
            return try watchJSONEncoder().encode(self)
        } catch {
            return Data()
        }
    }
    
    func copy(with files: [File], isLocal: Bool) -> AudioData {
         let content = AudioContent(subtitle: self.content.subtitle,
                                    description: self.content.description,
                                    cover: self.content.cover,
                                    primaryColor: self.content.primaryColor,
                                    secondaryColor: self.content.secondaryColor,
                                    files: files,
                                    hasBackgroundMusic: self.content.hasBackgroundMusic,
                                    title: self.content.title)
         
         return AudioData(content: content,
                          id: self.id,
                          title: self.title,
                          isLocal: isLocal)
     }

    // swiftlint:disable operator_whitespace
    static func ==(lhs: AudioData, rhs: AudioData) -> Bool {
        return lhs.id == rhs.id
    }
    // swiftlint:enable operator_whitespace
}
extension AudioData: NetworkingJSONDecodable {}

// MARK: - Content
struct AudioContent: Codable {
    let subtitle, description: String
    let cover: [Illustration]
    let primaryColor, secondaryColor: String?
    let files: [File]
    let hasBackgroundMusic: Bool
    let title: String

    enum CodingKeys: String, CodingKey {
        case subtitle
        case cover
        case description
        case primaryColor = "color_primary"
        case secondaryColor = "color_secondary"
        case files
        case hasBackgroundMusic = "background_sound"
        case title
    }
    
    func getPrimaryColor() -> String {
        guard let primaryColor = primaryColor else {
            return "FFFFFF"
        }
        let trimedPrimaryColor = primaryColor.replacingOccurrences(of: "#FF", with: "", options: [.regularExpression])

        return trimedPrimaryColor
    }
    
    func getSecondaryColor() -> String {
        guard let secondaryColor = secondaryColor else {
            return "000000"
        }
        let trimedPrimaryColor = secondaryColor.replacingOccurrences(of: "#FF", with: "", options: [.regularExpression])

        return trimedPrimaryColor
    }
    
    func getOrderedFiles() -> [String: [File]] {
        var dictionary = Dictionary(grouping: files, by: \.voice)
        for (key, value) in dictionary {
            dictionary[key] = value.sorted {$0.getLength() < $1.getLength() }
        }
        return dictionary
    }
    
    func getIllustrationUrl() -> String {
        return cover.first?.url ?? ""
    }
    
    func getDescription() -> String {
        description.stripOutHtml()
    }

}
extension AudioContent: NetworkingJSONDecodable {}

// MARK: - File
struct File: Codable, Identifiable, Equatable {
    let id: String
    let type: String
    let url: String
    let info: String
    
    func getCleanId() -> String {
        return id.replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "/", with: "-")
    }
    
    var voice: String {
        guard let infos = getSplitInfos() else {
            return "Unknown"
        }
        return String(infos[0])
    }
    
    func getLength() -> Int {
        guard let infos = getSplitInfos() else {
            return 0
        }
        var lenghtInMinutes = 0
        let elements = infos[1].split(separator: ":")
        if let hours = elements.first {
            lenghtInMinutes = (Int(hours) ?? 0) * 60
        }
        lenghtInMinutes += Int(elements[1]) ?? 0
        return lenghtInMinutes
    }
    
    func getUrl() -> URL? {
        return URL(string: url.replacingOccurrences(of: " ", with: "%20", options: [.regularExpression]))
    }
    // swiftlint:disable operator_whitespace
    static func ==(lhs: File, rhs: File) -> Bool {
        return lhs.id == rhs.id
    }
    // swiftlint:enable operator_whitespace
    
    private func getSplitInfos() -> [Substring]? {
        guard !info.isEmpty else {
            return nil
        }
        let infos = info.split(separator: ",")
        return infos.isEmpty ? nil : infos
    }
}
extension File: NetworkingJSONDecodable {}

// MARK: - Illustration
struct Illustration: Codable {
    let type: String
    let url: String
}
extension Illustration: NetworkingJSONDecodable {}

final class FavoriteAudioDataEntity: Object {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var audioData: Data = Data()
    @objc dynamic var isFav: Bool = false

   convenience init(id: String, data: Data) {
        self.init()
        self.id = "favorite:\(id)"
        audioData = data
    }
        
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func getId() -> String {
        let ids = id.components(separatedBy: ":")
        guard let realId = ids.last else {
            return ""
        }
        return realId
    }
}

final class OfflineAudioDataEntity: Object {
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var audioData: Data = Data()

    convenience init(id: String, data: Data) {
        self.init()
        self.id = "offline:\(id)"
        audioData = data
    }

    override static func primaryKey() -> String? {
        return "id"
    }

    func getId() -> String {
        let ids = id.components(separatedBy: ":")
        guard let realId = ids.last else {
            return ""
        }
        return realId
    }
}
