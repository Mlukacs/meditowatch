//
//  BackgroundSound.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 05/12/2020.
//

import Foundation

// MARK: - BackgroundSounds
struct BackgroundSoundsContainer: Codable {
    let backgroundSounds: [BackgroundSound]
    
    enum CodingKeys: String, CodingKey {
        case backgroundSounds = "data"
    }
}
extension BackgroundSoundsContainer: NetworkingJSONDecodable {}

// MARK: - BackgroundSound
struct BackgroundSound: Codable, Equatable {
    let content: SoundContent
    let extensionType, id: String
    let mime: String
    let name: String
    let size: Int
    let url: String

    enum CodingKeys: String, CodingKey {
        case content
        case extensionType = "extension"
        case id, mime, name, size, url
    }
    
    func getUrl() -> URL? {
        return URL(string: url.replacingOccurrences(of: " ", with: "%20", options: [.regularExpression]))
    }
    // swiftlint:disable operator_whitespace
    static func ==(lhs: BackgroundSound, rhs: BackgroundSound) -> Bool {
        return lhs.id == rhs.id
    }
    // swiftlint:enable operator_whitespace

}
extension BackgroundSound: NetworkingJSONDecodable {}

// MARK: - Content
struct SoundContent: Codable {
    let title, template: String

    enum CodingKeys: String, CodingKey {
        case title
        case template
    }
}
extension SoundContent: NetworkingJSONDecodable {}
