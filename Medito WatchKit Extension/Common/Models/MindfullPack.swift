//
//  Tile.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 01/12/2020.
//

import Foundation

// MARK: - Pack
struct MindfullPack: Codable, Identifiable {
    let type: String
    let display: Bool
    let title, subtitle: String?
    let itemPath: [ItemPath]
    let images: [ItemCover]?
    let primaryColor, secondaryColor: String?
    var id: String { itemPath.isEmpty ? UUID().uuidString : itemPath[0].id }

    enum CodingKeys: String, CodingKey {
          case type = "item_type"
          case display
          case title = "item_title"
          case subtitle = "item_subtitle"
          case itemPath = "item_path"
          case images = "item_cover"
          case primaryColor = "color_primary"
          case secondaryColor = "color_secondary"
      }
        
    func getTitle() -> String {
        guard !itemPath.isEmpty else {
            return title ?? ""
        }
        return itemPath[0].text
    }
    
    func getContentPath() -> String {
        guard !itemPath.isEmpty else {
            return ""
        }
        return itemPath[0].link
    }
    
    func getSubtitle() -> String {
        guard !itemPath.isEmpty else {
            return subtitle ?? ""
        }
        return itemPath[0].info
    }
    
    func getImageUrl() -> String {
        guard let imageArray = images, !imageArray.isEmpty else {
            return ""
        }
        return imageArray[0].url
    }
    
    func getPrimaryColor() -> String {
        guard let primaryColor = primaryColor, !primaryColor.isEmpty else {
            return "FFFFFF"
        }
        let trimedPrimaryColor = primaryColor.replacingOccurrences(of: "#FF", with: "", options: [.regularExpression])

        return trimedPrimaryColor
    }
    
    func getSecondaryColor() -> String {
        guard let secondaryColor = secondaryColor, !secondaryColor.isEmpty  else {
            return "000000"
        }
        let trimedSecondaryColor = secondaryColor.replacingOccurrences(of: "#FF", with: "", options: [.regularExpression])

        return trimedSecondaryColor
    }
}

extension MindfullPack: NetworkingJSONDecodable {}

// MARK: - ItemPath
struct ItemPath: Codable {
    let id: String
    let info, link, text: String
}
extension ItemPath: NetworkingJSONDecodable {}

// MARK: - ItemCover
struct ItemCover: Codable {
    let url: String
}

extension ItemCover: NetworkingJSONDecodable {}
