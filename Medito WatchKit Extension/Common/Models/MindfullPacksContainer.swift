//
//  Tiles.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 30/11/2020.
//

// MARK: - Home
struct MindfullPacksContainer: Codable {
    let data: DataClass
    
    func getPacks() -> [MindfullPack] {
        data.content.items.filter { !$0.itemPath.isEmpty }
    }
}
extension MindfullPacksContainer: NetworkingJSONDecodable {}

// MARK: - DataClass
struct DataClass: Codable {
    let content: ContentClass
    let id: String
}
extension DataClass: NetworkingJSONDecodable {}

struct ContentClass: Codable {
    let items: [MindfullPack]
    let title: String
}
extension ContentClass: NetworkingJSONDecodable {}
