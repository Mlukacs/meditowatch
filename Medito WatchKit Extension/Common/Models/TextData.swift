//
//  TextData.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 12/12/2020.
//

import Foundation

struct TextDataContainer: Codable {
    let data: TextData
}
extension TextDataContainer: NetworkingJSONDecodable {}

// MARK: - DataClass
struct TextData: Codable {
    let content: TextContent
    let id: String
    
    func getTitle() -> String {
        return content.title
    }
    
    func getBody() -> String {
        return content.body.replacingOccurrences(of: "\\\n", with: "\n")
    }
}
extension TextData: NetworkingJSONDecodable {}

// MARK: - Content
struct TextContent: Codable {
    let body, title: String

    enum CodingKeys: String, CodingKey {
        case body, title
    }
}
extension TextContent: NetworkingJSONDecodable {}
