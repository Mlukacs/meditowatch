//
//  UserContentTile.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 06/12/2020.
//

import SwiftUI

struct UserContentTile: Identifiable {
    let id: UUID
    let title: String
    let imageName: String
    let type: UserContentTileType
    let color: Color
}

enum UserContentTileType {
    case favorites
    case offline
    case settings
    case dataTracking
    case reminder
}
