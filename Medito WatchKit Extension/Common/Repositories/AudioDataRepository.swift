//
//  AudioData.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 02/12/2020.
//

import Combine
import SwiftUICombineToolBox
import Resolver

protocol AudioDataRepositoryContract {
    func audioData(for path: String) -> AnyPublisher<AudioData, Error>
}

final class AudioDataRepository: AudioDataRepositoryContract {
    @Injected private var api: AudioFileAPIContract
    private let cache = Cache<String, AudioData>()
    private var cancelBag = CancelBag()
    
    init() {}
    
    func audioData(for path: String) -> AnyPublisher<AudioData, Error> {
        if let cachedAudioData = cache[path] {
            return Just(cachedAudioData).switchToAnyPublisher(with: Error.self) 
        }
        
        return api.fetch(contentPath: path)
            .map { [weak self] audioContainer in
                self?.cache[path] = audioContainer.data
                return audioContainer.data
            }.eraseToAnyPublisher()
    }
}
