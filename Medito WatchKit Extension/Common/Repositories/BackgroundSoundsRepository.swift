//
//  BackgroundSoundsRepository.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 05/12/2020.
//

import Foundation
import Combine
import SwiftUICombineToolBox
import Resolver

protocol BackgroundSoundsRepositoryContract {
    var backgroundSoundsTitles: CurrentValueSubject<[String], Never> { get }
    func sound(for title: String) -> BackgroundSound?
}

final class BackgroundSoundsRepository: BackgroundSoundsRepositoryContract {
    @Injected private var api: BackgroundSoundsAPIContract
    private let cache = Cache<String, BackgroundSoundsContainer>()
    private var cancelBag = CancelBag()
    private var backgroundSounds: [String: BackgroundSound] = [:]
    private let cacheKey = "backgroundSounds"
    private var soundsName: [String] = []
    var backgroundSoundsTitles: CurrentValueSubject<[String], Never> = .init([])

    init() {
        setUp()
    }
    
    func sound(for title: String) -> BackgroundSound? {
         backgroundSounds[title]
    }
}

private extension BackgroundSoundsRepository {
    func setUp() {
        if let cachedSoundsContainer = cache[cacheKey] {
            setupData(with: cachedSoundsContainer)
            return
        }
        
        api.backgroundSounds()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    switch error {
                    case let decodingError as DecodingError:
                        print("\(decodingError.localizedDescription)")
                    // handle JSON decoding errors
                    case let networkingError as NetworkingError:
                        print("\(networkingError.localizedDescription)")
                    // handle NetworkingError
                    // print(networkingError.status)
                    // print(networkingError.code)
                    default:
                        print("\(error.localizedDescription)")
                    }
                }
            }) { [weak self] soundsContainer in
                guard let self = self else {
                    return
                }
                self.setupData(with: soundsContainer)
            }.store(in: &cancelBag)
    }
    
    func setupData(with container: BackgroundSoundsContainer) {
        var soundsNames: [String] = []
        soundsNames.append("None")
        for sound in container.backgroundSounds {
            backgroundSounds[sound.content.title] = sound
            soundsNames.append(sound.content.title)
        }
        backgroundSoundsTitles.send(soundsNames)
    }
}
