//
//  FavoritesRepository.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 05/12/2020.
//

import Foundation
import Combine
import SwiftUICombineToolBox
import Resolver

protocol FavoritesRepositoryContract {
    var favorites: CurrentValueSubject<[AudioData], Never> { get }
    func addToFavorite(for audio: AudioData)
    func removeFromFavorite(for audio: AudioData)
    func isInFavorites(for audio: AudioData) -> Bool
}

final class FavoritesRepository: FavoritesRepositoryContract {
    @Injected private var database: LocalStorageContract
    private var currentFav: [AudioData] = []
    var favorites: CurrentValueSubject<[AudioData], Never> = .init([])

    private var cancelBag = CancelBag()

    init() {
        setUp()
    }

    func addToFavorite(for audio: AudioData) {
        let entity = FavoriteAudioDataEntity(id: audio.id, data: audio.jsonData())
        database.createOrUpdate(for: entity)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("\(error.localizedDescription)")
                }
            }) { [weak self] _ in
                guard let self = self else {
                    return
                }
                self.currentFav.append(audio)
                self.favorites.send(self.currentFav)
            }.store(in: &cancelBag)
    }

    func removeFromFavorite(for audio: AudioData) {
        database.delete(type: FavoriteAudioDataEntity.self, with: "favorite:\(audio.id)")
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("\(error.localizedDescription)")
                }
            }) { [weak self] _ in
                guard let self = self else {
                    return
                }
                self.currentFav = self.currentFav.filter { $0.id != audio.id }
                self.favorites.send(self.currentFav)
            }.store(in: &cancelBag)
    }

    func updateFavorite(for audio: AudioData) {

    }
    
    func isInFavorites(for audio: AudioData) -> Bool {
        currentFav.contains(where: { $0.id == audio.id}) == true
    }
}

private extension FavoritesRepository {
    func setUp() {
        database.fetch(of: FavoriteAudioDataEntity.self)
            .receive(on: DispatchQueue.main)
            .map { objects -> [AudioData] in
                guard let audioDataEntities = objects as? [FavoriteAudioDataEntity] else {
                    return []
                }
                let audioDatas: [AudioData] = audioDataEntities.compactMap { audioDataEntitie in
                    do {
                        let audioData = try AudioData(data: audioDataEntitie.audioData)
                        return audioData
                    } catch {
                        return nil
                    }
                }
                return audioDatas
            }.sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("\(error.localizedDescription)")
                }
            }) { [weak self] audioData in
                guard let self = self else {
                    return
                }
                self.currentFav = audioData
                self.favorites.send(self.currentFav)
            }.store(in: &cancelBag)
    }
}
