//
//  TileRepositories.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 30/11/2020.
//

import Combine
import SwiftUICombineToolBox
import Resolver

protocol MindfullPackRepositoryContract {
    func packs(for path: String) -> AnyPublisher<MindfullPacksContainer, Error>
    func pack(for path: String) -> AnyPublisher<MindfullPack, Error>
}

final class MindfullPackRepository: MindfullPackRepositoryContract {
    @Injected private var api: PackAPIContract
    private let cacheForPacksContainers = Cache<String, MindfullPacksContainer>()
    private let cacheForPacks = Cache<String, MindfullPack>()
    private var cancelBag = CancelBag()
    
    init() {}
    
    func packs(for path: String) -> AnyPublisher<MindfullPacksContainer, Error> {
        if let cachedPacks = cacheForPacksContainers[path] {
            return Just(cachedPacks).switchToAnyPublisher(with: Error.self)
        }
        
        return api.packs(for: path)
            .map { [weak self] packsContainer in
                self?.cacheForPacksContainers[path] = packsContainer
                return packsContainer
            }.eraseToAnyPublisher()
    }
    
    func pack(for path: String) -> AnyPublisher<MindfullPack, Error> {
        if let cachedPack = cacheForPacks[path] {
            return Just(cachedPack).switchToAnyPublisher(with: Error.self) 
        }
        
        return api.fetchPack(for: path)
            .map { [weak self] pack in
                self?.cacheForPacks[path] = pack
                return pack
            }.eraseToAnyPublisher()
    }
}

private extension MindfullPackRepository {}
