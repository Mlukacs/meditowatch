//
//  OfflineRepository.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 02/01/2021.
//

import Foundation
import Combine
import SwiftUICombineToolBox
import Resolver

protocol LocalRepositoryContract {
    var localContent: CurrentValueSubject<[AudioData], Never> { get }
    var downloadInformation: CurrentValueSubject<PlayableStatus, Never> { get }
    var downloadProgression: PassthroughSubject<Float, Never> { get }

    func addToLocalData(for audio: AudioData)
    func cancelLocalPersistance()
    func removeFromLocalData(for audio: AudioData)
    func isOnDevice(for audio: AudioData) -> Bool
}

final class LocalRepository: LocalRepositoryContract {
    @Injected private var database: LocalStorageContract
    @Injected private var downloader: PersistenceContract
    private var currentLocal: [AudioData] = []
    var localContent: CurrentValueSubject<[AudioData], Never> = .init([])
    var downloadInformation: CurrentValueSubject<PlayableStatus, Never> = .init((data: nil, status: .idle))
    var downloadProgression: PassthroughSubject<Float, Never> = .init()
    private var cancelBag = CancelBag()

    init() {
        setUp()
    }

    func addToLocalData(for audio: AudioData) {
        downloader.download(for: audio)
    }

    func removeFromLocalData(for audio: AudioData) {
        database.delete(type: OfflineAudioDataEntity.self, with: "offline:\(audio.id)")
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("\(error.localizedDescription)")
                }
            }) { [weak self] _ in
                guard let self = self else {
                    return
                }
                self.downloader.deleteLocalData(for: audio)
                self.currentLocal = self.currentLocal.filter { $0.id != audio.id }
                self.localContent.send(self.currentLocal)
            }.store(in: &cancelBag)
    }

    func cancelLocalPersistance() {
        downloader.cancelDownload()
    }

    func isOnDevice(for audio: AudioData) -> Bool {
        currentLocal.contains(where: { $0.id == audio.id }) == true
    }
}

private extension LocalRepository {
    func setUp() {
        database.fetch(of: OfflineAudioDataEntity.self)
            .receive(on: DispatchQueue.main)
            .map { objects -> [AudioData] in
                guard let audioDataEntities = objects as? [OfflineAudioDataEntity] else {
                    return []
                }
                let audioDatas: [AudioData] = audioDataEntities.compactMap { audioDataEntitie in
                    do {
                        var audioData = try AudioData(data: audioDataEntitie.audioData)
                        audioData.isLocal = true
                        return audioData
                    } catch {
                        return nil
                    }
                }
                return audioDatas
            }.sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("\(error.localizedDescription)")
                }
            }) { [weak self] localAudioData in
                guard let self = self else {
                    return
                }
                self.currentLocal = localAudioData
                self.currentLocal = self.currentLocal.sorted {
                    $0.title < $1.title
                }
                self.localContent.send(self.currentLocal)
            }.store(in: &cancelBag)

        downloader.downloadInformation
            .receive(on: DispatchQueue.main)
            .filter { $0.status == .finished }
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    print("\(error.localizedDescription)")
                }
            }) { [weak self] downloadedAudioData in
                guard let self = self,
                      let data = downloadedAudioData.data else { return }
                self.saveMetadata(for: data)
            }.store(in: &cancelBag)

        downloadInformation = downloader.downloadInformation
        downloadProgression = downloader.downloadProgression
    }

    func saveMetadata(for audio: AudioData) {
        let entity = OfflineAudioDataEntity(id: audio.id, data: audio.jsonData())
        database.createOrUpdate(for: entity)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break
                case .failure(let error):
                    // todo delete local files
                    print("\(error.localizedDescription)")
                }
            }) { [weak self] _ in
                guard let self = self else {
                    return
                }
                self.appendAndShare(for: audio)
            }.store(in: &cancelBag)
    }
    
    func appendAndShare(for audio: AudioData) {
        currentLocal.append(audio)
        currentLocal = currentLocal.sorted {
            $0.title < $1.title
        }
        localContent.send(currentLocal)
    }
}
