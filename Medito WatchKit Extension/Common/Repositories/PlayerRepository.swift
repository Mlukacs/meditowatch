//
//  PlayerRepository.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 12/12/2020.
//

import Foundation
import Combine
import AVFoundation
import SwiftUICombineToolBox
import Resolver

enum PlayerStateStatus: Equatable {
    case playing
    case paused
    case buffering
    case error
}

protocol PlayerRepositoryContract {
    var currentAudioDataPublisher: CurrentValueSubject<AudioData?, Never> { get set }
    var currentFilePublisher: CurrentValueSubject<File?, Never> { get set }
    var currentBackgroundSoundPublisher: CurrentValueSubject<BackgroundSound?, Never> { get set }
    var currentTimePublisher: CurrentValueSubject<String, Never> { get set }
    var currentProgressPublisher: CurrentValueSubject<Float, Never> { get set }
    var currentTotalDuration: CurrentValueSubject<String, Never> { get set }
    var playerCurrentStatus: CurrentValueSubject<PlayerStateStatus, Never> { get set }
    var isCurrentlyLoading: CurrentValueSubject<Bool, Never> { get set }
    var isBackgroundSoundPlaying: CurrentValueSubject<Bool, Never> { get set }
    var hasContentLoaded: CurrentValueSubject<Bool, Never> { get set }
    
    func loadPlayers(with data: AudioData?, audioFile: File?, backgroundSound: BackgroundSound?)
    func togglePlaying()
    func toggleBackgroundSoundPlaying()
    func dataAlreadyLoaded(with data: AudioData?, audioFile: File?, backgroundSound: BackgroundSound?) -> Bool
    func seek(by time: Double, and direction: SeekDirection)
    func startPlaying()
}

final class PlayerRepository: PlayerRepositoryContract {
    var currentTimePublisher: CurrentValueSubject<String, Never> = .init("00.00")
    var currentProgressPublisher: CurrentValueSubject<Float, Never> = .init(0)
    var currentTotalDuration: CurrentValueSubject<String, Never> = .init("00.00")
    var isCurrentlyLoading: CurrentValueSubject<Bool, Never> = .init(false)
    var playerCurrentStatus: CurrentValueSubject<PlayerStateStatus, Never> = .init(.paused)
    var currentAudioDataPublisher: CurrentValueSubject<AudioData?, Never> = .init(nil)
    var currentFilePublisher: CurrentValueSubject<File?, Never> = .init(nil)
    var currentBackgroundSoundPublisher: CurrentValueSubject<BackgroundSound?, Never> = .init(nil)
    var isBackgroundSoundPlaying: CurrentValueSubject<Bool, Never> = .init(false)
    var hasContentLoaded: CurrentValueSubject<Bool, Never> = .init(false)

    private var audioData: AudioData?
    private var selectedFile: File?
    private var selectedSound: BackgroundSound?
    private var mainPlayer: AVPlayer
    private var backGroundPlayer: AVQueuePlayer
    private var cancelBag = CancelBag()
    private var timeObserverToken: Any?
    private var fadeTimer: Timer?
    @Injected private var healthKitService: HealthKitServiceContract
    
    init() {
        mainPlayer = AVPlayer()
        backGroundPlayer = AVQueuePlayer()
        playerSetUp()
    }
    
    deinit {
        cleanPlayer()
        NotificationCenter.default.removeObserver(self)
    }

    func loadPlayers(with data: AudioData?, audioFile: File?, backgroundSound: BackgroundSound?) {
        
        cleanPlayers()
        audioData = data
        selectedFile = audioFile
        selectedSound = backgroundSound
        currentAudioDataPublisher.send(audioData)
        currentFilePublisher.send(selectedFile)
        currentBackgroundSoundPublisher.send(selectedSound)
        if audioFile != nil || backgroundSound != nil {
            hasContentLoaded.send(true)
        }
        healthKitService.stopMeditation()
        isCurrentlyLoading.send(true)
        Publishers.CombineLatest(avPlayerItemPublisher(for: selectedFile?.getUrl()),
                                 avPlayerItemPublisher(for: selectedSound?.getUrl()))
            .subscribe(on: DispatchQueue.global(qos: .userInteractive))
            .receive(on: DispatchQueue.main)
            .share()
            .sink(receiveCompletion: { [weak self] completion in
                switch completion {
                case .finished:
                    self?.isCurrentlyLoading.send(false)
                    return
                case .failure:
                    self?.isCurrentlyLoading.send(false)
                    return
                }
            }) { [weak self] audioFile, sound  in
                self?.isCurrentlyLoading.send(false)
                guard let self = self else {
                    return
                }
                let totalDuration = audioFile?.duration.seconds.timeDisplay() ?? "00.00"
                self.currentTotalDuration.send(totalDuration)
                self.mainPlayer.replaceCurrentItem(with: audioFile)
                self.backGroundPlayer.replaceCurrentItem(with: sound)
            }.store(in: &cancelBag)
    }
    
    func togglePlaying() {
        guard mainPlayer.currentItem != nil else {
            return
        }
        if mainPlayer.isPlaying {
            mainPlayer.pause()
            backGroundPlayer.pause()
        } else {
            mainPlayer.play()
            backGroundPlayer.play()
        }
    }
    
    func toggleBackgroundSoundPlaying() {
        guard backGroundPlayer.currentItem != nil else {
            return
        }
        if backGroundPlayer.isPlaying {
            isBackgroundSoundPlaying.send(false)
            backGroundPlayer.pause()
        } else {
            isBackgroundSoundPlaying.send(true)
            backGroundPlayer.play()
        }
    }
    
    func dataAlreadyLoaded(with data: AudioData?,
                           audioFile: File?,
                           backgroundSound: BackgroundSound?) -> Bool {
        if audioData == nil && selectedFile == nil && selectedSound == nil {
            return false
        }

        return data == audioData
            && audioFile == selectedFile
            && backgroundSound == selectedSound
    }
    
    func seek(by time: Double, and direction: SeekDirection) {
        guard let item = mainPlayer.currentItem else {
            return
        }
        let currentTime = mainPlayer.currentTime().seconds
        var seekTime = currentTime
        var time2: CMTime!
        switch direction {
        case .forward:
            seekTime = currentTime + time > item.duration.seconds ? item.duration.seconds : currentTime + time
            time2 = CMTimeMake(value: Int64(seekTime * 1000 as Float64), timescale: 1000)
        case .backward:
            seekTime = currentTime - time < 0 ? 0 : currentTime - time
            time2 = CMTimeMake(value: Int64(seekTime * 1000 as Float64), timescale: 1000)
        }
        mainPlayer.seek(to: time2, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
    }
    
    func startPlaying() {
        mainPlayer.play()
        backGroundPlayer.play()
        setPlayingState(playing: true)
    }
}

private extension PlayerRepository {

    func playerSetUp() {
        mainPlayer.automaticallyWaitsToMinimizeStalling = false
        backGroundPlayer.automaticallyWaitsToMinimizeStalling = false
        backGroundPlayer.volume = 0.3
        setUpNotification()
        setUpPlayerObservers()
    }
    
    // MARK: - Notifications
    
    func setUpNotification() {
        let nc = NotificationCenter.default
        nc.addObserver(self, selector: #selector(playbackDidFinishForAudio),
                       name: .AVPlayerItemDidPlayToEndTime,
                       object: mainPlayer.currentItem)
        nc.addObserver(self, selector: #selector(playbackDidFinishForSound),
                       name: .AVPlayerItemDidPlayToEndTime,
                       object: backGroundPlayer.currentItem)
    }
    
    @objc func playbackDidFinishForAudio(_ noti: Notification) {
        guard let item = noti.object as? AVPlayerItem,
              item == mainPlayer.currentItem else {
            return
        }
        backGroundPlayer.pause()
        mainPlayer.pause()
        resetTimers()
        mainPlayer.seek(to: CMTime.zero)
        backGroundPlayer.seek(to: CMTime.zero)
    }
    
    @objc func playbackDidFinishForSound(_ noti: Notification) {
        guard backGroundPlayer.currentItem != nil,
              let item = noti.object as? AVPlayerItem,
              item == backGroundPlayer.currentItem else {
            return
        }
        backGroundPlayer.seek(to: CMTime.zero)
        backGroundPlayer.play()
    }
    
    // MARK: - Player Observers
    func setUpPlayerObservers() {
        setupPeriodicObservation()

        mainPlayer.publisher(for: \.timeControlStatus)
            .sink(receiveValue: { [weak self] status in
                switch status {
                case .playing:
                    self?.playerCurrentStatus.send(.playing)
                    self?.setPlayingState(playing: true)
                    self?.healthKitService.startMeditation()
                    if self?.backGroundPlayer.currentItem != nil {
                        self?.backGroundPlayer.play()
                    }
                case .waitingToPlayAtSpecifiedRate:
                    self?.playerCurrentStatus.send(.buffering)
                    self?.setPlayingState(playing: false)
                case.paused:
                    self?.playerCurrentStatus.send(.paused)
                    self?.healthKitService.stopMeditation()
                    self?.setPlayingState(playing: false)
                    if self?.backGroundPlayer.currentItem != nil {
                        self?.backGroundPlayer.pause()
                    }
                @unknown default:
                    print("wait and see")
                }
            })
            .store(in: &cancelBag)
    }
    
    func setupPeriodicObservation() {
        let timeScale = CMTimeScale(NSEC_PER_SEC)
        let time = CMTime(seconds: 1, preferredTimescale: timeScale)
        
        timeObserverToken = mainPlayer.addPeriodicTimeObserver(forInterval: time, queue: .main) { [weak self] (time) in
            guard let self = self else { return }
            let progress = self.calculateProgress(currentTime: time.seconds)
            let newTime = time.seconds.timeDisplay()
            self.currentProgressPublisher.send(progress)
            self.currentTimePublisher.send(newTime)
            guard let duration = self.mainPlayer.currentItem?.duration.seconds,
                  time.seconds >= duration - 10,
                  self.fadeTimer == nil else {
                return
            }
            self.fadeTimer = self.backGroundPlayer.fadeVolume(from: self.backGroundPlayer.volume, to: 0, duration: 7)
        }
    }
}

// MARK: - Utils

private extension PlayerRepository {
    func resetTimers() {
        currentProgressPublisher.send(0)
        currentTimePublisher.send("00.00")
        backGroundPlayer.volume = mainPlayer.volume * 0.3
        fadeTimer?.invalidate()
        fadeTimer = nil
    }
    
    func setPlayingState(playing: Bool) {
        playerCurrentStatus.send(playing ? .playing : .paused) 
        isBackgroundSoundPlaying.send(playing)
    }
    
    func calculateProgress(currentTime: Double) -> Float {
        guard let duration = self.mainPlayer.currentItem?.duration.seconds else {
            return 0
        }
        let value = Float(currentTime / duration)
        return value.isNaN ? 0 : value
    }
    
    func cleanPlayer() {
        if let timeObserverToken = timeObserverToken {
            mainPlayer.removeTimeObserver(timeObserverToken)
            self.timeObserverToken = nil
        }
        cleanPlayers()
        healthKitService.stopMeditation()
    }
    
    func cleanPlayers() {
        mainPlayer.pause()
        backGroundPlayer.pause()
        mainPlayer.replaceCurrentItem(with: nil)
        backGroundPlayer.removeAllItems()
        resetTimers()
        currentTotalDuration.send("00.00")
        healthKitService.stopMeditation()
    }
    
    func avPlayerItemPublisher(for mediaUrl: URL?) -> AnyPublisher<AVPlayerItem?, Never> {
        return Deferred {
            Future<AVPlayerItem?, Never> {  promise in
                guard let mediaUrl = mediaUrl else {
                    return promise(.success(nil))
                }
                let asset = AVURLAsset(url: mediaUrl)
                // load values asynchronously and once complete, create the player item
                let keys = ["playable", "duration", "tracks"]
                asset.loadValuesAsynchronously(forKeys: keys) {
                    guard asset.isPlayable else {
                        return promise(.success(nil))
                    }
                    let currentItem = AVPlayerItem(asset: asset, automaticallyLoadedAssetKeys: nil)
                    promise(.success(currentItem))
                }
            }
        }.eraseToAnyPublisher()
    }
}
