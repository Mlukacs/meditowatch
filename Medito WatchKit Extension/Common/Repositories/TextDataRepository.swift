//
//  TextDataRepository.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 12/12/2020.
//

import Combine
import SwiftUICombineToolBox
import Resolver

protocol TextDataRepositoryContract {
    func textData(for path: String) -> AnyPublisher<TextData, Error>
}

final class TextDataRepository: TextDataRepositoryContract {
    @Injected private var api: TextAPIContract
    private let cache = Cache<String, TextData>()
    private var cancelBag = CancelBag()
    
    init() {}
    
    func textData(for path: String) -> AnyPublisher<TextData, Error> {
        if let cachedTextData = cache[path] {
            return Just(cachedTextData).switchToAnyPublisher(with: Error.self)
        }
        
        return api.fetch(contentPath: path)
            .map { [weak self] textContainer in
                self?.cache[path] = textContainer.data
                return textContainer.data
            }.eraseToAnyPublisher()
    }
}
