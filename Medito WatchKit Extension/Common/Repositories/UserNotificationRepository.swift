//
//  UserNotificationRepository.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 23/01/2021.
//

import Foundation
import UserNotifications
import Combine
import Resolver

enum ReminderType: String, CaseIterable {
    case daily = "Daily"
    case sleep = "Sleep"
    case sound = "Sound"
    case timer = "Timer"
}

enum NotificationConfig {
    static let notificationCategory = "MeditoAlarm"
    static let actionCategory = "DiveInSession"
}

enum NotificationPermissionError: Error {
    case noPermission
}

protocol UserNotificationRepositoryContract {
    func askPermission() -> AnyPublisher<Bool, NotificationPermissionError>
    func scheduleNotification(for type: ReminderType, hour: Int, minutes: Int)
    func fetchAllActiveReminder() -> AnyPublisher<[UNNotificationRequest], Never>
    func clearAll()
    func removeRemidner(with identifier: String)
}

final class UserNotificationRepository: NSObject, UserNotificationRepositoryContract, UNUserNotificationCenterDelegate {
        
    private let center = UNUserNotificationCenter.current()
    @Injected private var router: RouteToPageContract

    override init() {
        super.init()
        center.delegate = self
    }
    
    func askPermission() -> AnyPublisher<Bool, NotificationPermissionError> {
        Deferred {
            Future <Bool, NotificationPermissionError> { [weak self] promise in
                self?.center.requestAuthorization(options: [.alert, .badge, .sound]) { (granted, _) in
                    if granted {
                        promise(.success(granted))
                    } else {
                        promise(.failure(.noPermission))
                    }
                }
            }
        }.eraseToAnyPublisher()
    }
    
    func scheduleNotification(for type: ReminderType, hour: Int, minutes: Int) {

        let content = UNMutableNotificationContent()
        content.title = "Time for a little Meditation"
        content.body = "Your treasures lie within."
        content.categoryIdentifier = NotificationConfig.notificationCategory
        content.userInfo = ["MeditoReminderType": type.rawValue]
        content.sound = UNNotificationSound.default

        var dateComponents = DateComponents()
        dateComponents.hour = hour
        dateComponents.minute = minutes
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)

        center.add(request)
        
        let show = UNNotificationAction(identifier: NotificationConfig.actionCategory, title: "Go strait to your session", options: .foreground)
        let category = UNNotificationCategory(identifier: NotificationConfig.notificationCategory, actions: [show], intentIdentifiers: [])

        center.setNotificationCategories([category])
    }

    func fetchAllActiveReminder() -> AnyPublisher<[UNNotificationRequest], Never> {
        Deferred {
            Future <[UNNotificationRequest], Never> { [weak self] promise in
                self?.center.getPendingNotificationRequests { request in
                    let parsedRequest = request.filter { $0.content.categoryIdentifier == Config.notificationCathegory}
                    promise(.success(parsedRequest))
                }
            }
        }.eraseToAnyPublisher()
    }
    
    func clearAll() {
        center.removeAllPendingNotificationRequests()
    }
    
    func removeRemidner(with identifier: String) {
        center.removePendingNotificationRequests(withIdentifiers: [identifier])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {

        switch response.actionIdentifier {
        case NotificationConfig.actionCategory:
            // pull out the buried userInfo dictionary
            let userInfo = response.notification.request.content.userInfo
            
            if let destinationDescriber = userInfo["MeditoReminderType"] as? String {
                router.navigateToDeeplinkType(for: destinationDescriber)
            }
        default:
            break
        }

        // you must call the completion handler when you're done
        completionHandler()
    }
}
