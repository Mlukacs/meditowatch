//
//  AudioDataTile.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 06/12/2020.
//

import SwiftUI
import Kingfisher
import KingfisherSwiftUI

struct AudioDataRow: View {
    var data: AudioData
    
    var body: some View {
            base
                .cornerRadius(5.0)
    }
}

private extension AudioDataRow {
    var base : some View {
        HStack {
            coverImage
            Spacer()
            infoContainer
            Spacer()
        }
    }
}

// MARK: - Cover Image
private extension AudioDataRow {
    var coverImage : some View {
        KFImage(URL(string: data.content.getIllustrationUrl()),
                options: [
                    .processor(
                        DownsamplingImageProcessor(size: CGSize(width: 100, height: 100))
                    ),
                    .cacheOriginalImage
                ])
            .cancelOnDisappear(true)
            .resizable()
            .placeholder {
                Image("icon_ic_logo")
            }
            .frame(width: 30, height: 30)
            .aspectRatio(contentMode: .fit)
            .background(Color(hex: data.content.getPrimaryColor()))
            .cornerRadius(5.0)
            .padding(.leading, 5)
    }
}

// MARK: - Playlist information display
private extension AudioDataRow {
    var infoContainer: some View {
        VStack(alignment: .center) {
            HStack {
                Text(data.title)
                    .fontWeight(.semibold)
                    .font(.system(size: 14))
                    .lineLimit(2)
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
            }
        }
    }
}
