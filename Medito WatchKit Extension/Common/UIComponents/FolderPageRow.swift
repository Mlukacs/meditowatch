//
//  ListPageRow.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 01/12/2020.
//

import SwiftUI
import Kingfisher
import KingfisherSwiftUI

struct FolderPageRow: View {
    var pack: MindfullPack
    
    var body: some View {
            base
    }
}

private extension FolderPageRow {
    var base : some View {
        HStack {
            if pack.type == "folder" || pack.type == "article" || pack.type == "text" {
                filesCoverImage
            } else {
                audioCoverImage
            }
            Spacer()
            infoContainer
            Spacer()
        }
    }
}

// MARK: - Cover Image
private extension FolderPageRow {
    var filesCoverImage : some View {
        var icon = "icon_ic_logo"
        switch pack.type {
        case "text", "article":
            icon = "ic_document"
        default:
            icon = "ic_folder"
        }

       return Image(icon)
            .resizable()
            .renderingMode(.template)
            .foregroundColor(Color.lightTextColor)
            .frame(width: 30, height: 30)
            .aspectRatio(contentMode: .fit)
            .cornerRadius(5.0)
            .padding(.leading, 5)
    }
}
// MARK: - Cover Image
private extension FolderPageRow {
    var audioCoverImage : some View {
        Image(systemName: "headphones")
            .resizable()
            .frame(width: 25, height: 25, alignment: .center)
            .aspectRatio(contentMode: .fit)
            .cornerRadius(5.0)
            .padding(.leading, 5)
            .padding(.trailing, 5)
    }
}

// MARK: - Playlist information display
private extension FolderPageRow {
    var infoContainer: some View {
        VStack(alignment: .leading) {
            HStack {
                Text(pack.getTitle())
                    .fontWeight(.semibold)
                    .font(.system(size: 14))
                    .lineLimit(2)
                    .foregroundColor(.white)
                Spacer()
            }
            if pack.type != "folder" && pack.type != "text" && pack.type != "article" && !pack.getSubtitle().isEmpty {
                HStack {
                    Text(pack.getSubtitle())
                        .fontWeight(.light)
                        .font(.system(size: 11))
                        .lineLimit(2)
                        .foregroundColor(Color(hex: pack.getPrimaryColor()))
                }
            }
        }
    }
}
