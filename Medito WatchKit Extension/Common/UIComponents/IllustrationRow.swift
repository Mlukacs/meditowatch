//
//  IllustrationRow.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 01/12/2020.
//

import SwiftUI
import Kingfisher
import KingfisherSwiftUI

struct IllustrationRow: View {
    var pack: MindfullPack
    
    var body: some View {
        base
    }
}

private extension IllustrationRow {
    var base : some View {
        HStack {
            coverImage
        }
    }
}

// MARK: - Cover Image
private extension IllustrationRow {
    var coverImage : some View {
        KFImage(URL(string: pack.getImageUrl()),
                options: [
                    .processor(
                        DownsamplingImageProcessor(size: CGSize(width: 400, height: 400))
                    ),
                    .cacheOriginalImage
                ])
            .cancelOnDisappear(true)
            .resizable()
            .placeholder {
                Image("icon_ic_logo")
            }
            .aspectRatio(contentMode: .fit)
            .cornerRadius(5.0)
            .padding(.leading, 5)
    }
}
