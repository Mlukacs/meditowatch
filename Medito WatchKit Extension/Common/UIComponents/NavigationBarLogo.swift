//
//  NavigationBarLogo.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 01/12/2020.
//

import SwiftUI

struct NavigationBarLogo: View {
    var iconeName: String
    
    var body: some View {
        
        let size = UIImage(named: iconeName)?.size ?? CGSize(width: 172, height: 36)
        let aspect = size.width / size.height
        HStack {
            Image(iconeName)
                .resizable()
                .aspectRatio(aspect, contentMode: .fit)
                .frame(width: 75, height: 22, alignment: .leading)
                Spacer()
        }
        .background(Color.clear)
    }
}
