//
//  TileRow.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 01/12/2020.
//

import SwiftUI
import Kingfisher
import KingfisherSwiftUI

struct PackRow: View {
    var pack: MindfullPack
    var shoudlPresentSub: Bool = false

    var body: some View {
        base
            .background(Color(hex: pack.getPrimaryColor()))
            .cornerRadius(5.0)
    }
}

private extension PackRow {
    var base : some View {
        HStack {
            if !pack.getImageUrl().isEmpty {
                coverImage
            }
            infoContainer
            Spacer()
        }
    }
}

// MARK: - Cover Image
private extension PackRow {
    var coverImage : some View {
        KFImage(URL(string: pack.getImageUrl()),
                options: [
                    .processor(
                        DownsamplingImageProcessor(size: CGSize(width: 100, height: 100))
                    ),
                    .cacheOriginalImage
                ])
            .cancelOnDisappear(true)
            .resizable()
            .placeholder {
                Image("icon_ic_logo")
            }
            .frame(width: 40, height: 40)
            .aspectRatio(contentMode: .fit)
            .cornerRadius(5.0)
            .padding(.top, 3)
            .padding(.bottom, 3)
            .padding(.leading, 3)
}
}

// MARK: - Playlist information display
private extension PackRow {
    var infoContainer: some View {
        VStack(alignment: .center) {
                HStack(alignment: .center) {
                    Spacer()
                    Text(pack.getTitle())
                        .fontWeight(.semibold)
                        .font(.system(size: 13))
                        .lineLimit(3)
                        .multilineTextAlignment(.center)
                        .minimumScaleFactor(0.85)
                        .foregroundColor(Color(hex: pack.getSecondaryColor()))
                        .padding(.top, 3)
                        .padding(.bottom, 3)
                        .padding(.leading, 2)
                        .padding(.trailing, 2)
                    Spacer()
                }
            if !pack.getSubtitle().isEmpty && shoudlPresentSub {
                HStack {
                    Text(pack.getSubtitle())
                        .fontWeight(.light)
                        .font(.system(size: 11))
                        .lineLimit(3)
                        .foregroundColor(Color(hex: pack.getSecondaryColor()))
                        .multilineTextAlignment(.center)
                        .padding(.leading, 5)
                        .padding(.trailing, 5)
                        .padding(.bottom, 5)
                }
            }
        }
    }
}
