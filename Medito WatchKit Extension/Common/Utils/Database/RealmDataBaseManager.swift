//
//  RealmDataBaseManager.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 06/12/2020.
//

import Foundation
import Combine
import RealmSwift

enum LocalStorageErrors: Error {
    case generic(description: String)
}

protocol LocalStorageContract {
    func fetch(of objectType: Object.Type) -> AnyPublisher<[Object], LocalStorageErrors>
    func createOrUpdate(for object: Object) -> AnyPublisher<Bool, LocalStorageErrors>
    func delete(type: Object.Type, with primaryKey: String) -> AnyPublisher<Bool, LocalStorageErrors>
    func clearAllData() -> AnyPublisher<Bool, LocalStorageErrors>
}

final class RealmLocalStorageService: LocalStorageContract {
    private let realm: Realm
    
    // swiftlint:disable force_try
    init(realm: Realm = try! Realm()) {
        self.realm = realm
    }
    // swiftlint:enable force_try
    
    func createOrUpdate(for object: Object) -> AnyPublisher<Bool, LocalStorageErrors> {
        Deferred {
            Future <Bool, LocalStorageErrors> { [weak self] promise in
                do {
                    try self?.realm.write {
                        self?.realm.add(object, update: .modified)
                    }
                    promise(.success(true))
                } catch let error {
                    promise(.failure(.generic(description: error.localizedDescription)))
                }
            }
        }.eraseToAnyPublisher()
    }
    
    func fetch(of objectType: Object.Type) -> AnyPublisher<[Object], LocalStorageErrors> {
           return realm.objects(objectType).collectionPublisher
            .freeze()
            .map { objects in
                Array(objects)
            }.mapError { error in
                .generic(description: error.localizedDescription)
            }.eraseToAnyPublisher()
    }

    func delete(type: Object.Type, with primaryKey: String) -> AnyPublisher<Bool, LocalStorageErrors> {
        Deferred {
            Future <Bool, LocalStorageErrors> { [weak self] promise in
                guard let object = self?.realm.object(ofType: type, forPrimaryKey: primaryKey) else {
                    promise(.failure(.generic(description: "No object found")))
                    return
                }
                do {
                    try self?.realm.write {
                        self?.realm.delete(object)
                    }
                    promise(.success(true))
                } catch let error {
                    promise(.failure(.generic(description: error.localizedDescription)))
                }
            }
        }.eraseToAnyPublisher()
    }
    
    func clearAllData() -> AnyPublisher<Bool, LocalStorageErrors> {
        Deferred {
            Future <Bool, LocalStorageErrors> { [weak self] promise in
                do {
                    try self?.realm.write {
                        self?.realm.deleteAll()
                    }
                    promise(.success(true))
                } catch let error {
                    promise(.failure(.generic(description: error.localizedDescription)))
                }
            }
        }.eraseToAnyPublisher()
    }
}
