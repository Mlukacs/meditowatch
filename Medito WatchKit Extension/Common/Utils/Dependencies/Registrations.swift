//
//  Registrations.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 24/01/2021.
//

import Resolver

extension Resolver: ResolverRegistering {
    public static func registerAllServices() {
        registerTools()
        registerRepositories()
        registerRouting()
        registerViews()
        registerViewModels()
    }
}
