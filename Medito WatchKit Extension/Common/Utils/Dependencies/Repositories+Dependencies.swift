//
//  Repositories+Dependencies.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 24/01/2021.
//

import Resolver

extension Resolver {

    public static func registerRepositories() {
        register { MindfullPackRepository() as MindfullPackRepositoryContract }.scope(.application)
        register { AudioDataRepository() as AudioDataRepositoryContract }.scope(.application)
        register { BackgroundSoundsRepository() as BackgroundSoundsRepositoryContract }.scope(.application)
        register { FavoritesRepository() as FavoritesRepositoryContract }.scope(.application)
        register { TextDataRepository() as TextDataRepositoryContract}.scope(.application)
        register { PlayerRepository() as PlayerRepositoryContract }.scope(.application)
        register { LocalRepository() as LocalRepositoryContract }.scope(.application)
        register { UserNotificationRepository() as UserNotificationRepositoryContract }.scope(.application)
    }
}
