//
//  Routing+Dependencies.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 24/01/2021.
//

import Resolver

extension Resolver {

    public static func registerRouting() {
        register { MainRouter() }
            .implements(RouteToPageContract.self)
            .implements(RouteToUserPageContract.self)
            .scope(.application)
    }
}
