//
//  Tools+Dependencies.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 24/01/2021.
//

import Resolver

extension Resolver {

    public static func registerTools() {
        register { API() }
            .implements(PackAPIContract.self)
            .implements(AudioFileAPIContract.self)
            .implements(BackgroundSoundsAPIContract.self)
            .implements(TextAPIContract.self)
            .scope(.application)
        register { ExtendedSession() as ExtendedSessionContract }.scope(.application)
        register { RealmLocalStorageService() as LocalStorageContract }.scope(.application)
        register { HealthKitService() as HealthKitServiceContract }.scope(.application)
        register { Downloader() as PersistenceContract }.scope(.application)
    }
}
