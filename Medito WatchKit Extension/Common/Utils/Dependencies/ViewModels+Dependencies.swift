//
//  ViewModels+Dependencies.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 24/01/2021.
//

import Resolver

extension Resolver {

    public static func registerViewModels() {
        register { HomeViewModel() }.scope(.application)
        register { AudioPageViewModel() }.scope(.application)
        register { UserContentHomeViewModel() }
        register { FavoritesViewModel() }
        register { PlayerPageViewModel() }.scope(.application)
        register { TextPageViewModel() }
        register { MainTabViewModel() }.scope(.application)
        register { OfflineViewModel() }
        register { StatisticViewModel() }
        register { ReminderViewModel() }
        register { ReminderCreationModel() }
        register { FolderPageViewModel() }
    }
}
