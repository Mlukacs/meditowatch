//
//  Views+Dependencies.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 24/01/2021.
//

import SwiftUI
import Resolver

extension Resolver {

    public static func registerViews() {
        register { VolumeView() }.scope(.application)
    }
}
