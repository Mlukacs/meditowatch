//
//  ExtendedSessionService.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 08/12/2020.
//

import WatchKit
import SwiftUICombineToolBox

protocol ExtendedSessionContract {
    func start()
    func stop()
}

final class ExtendedSession: ExtendedSessionContract {
    private var extendedSession: WKExtendedRuntimeSession?
    private var cancelBag = CancelBag()

    init() {
        setUp()
    }
    
    func start() {
        guard extendedSession == nil else {
            return
        }
        extendedSession = WKExtendedRuntimeSession()
        extendedSession?.start()
    }
    
    func stop() {
        extendedSession?.invalidate()
        extendedSession = nil
    }
}

private extension ExtendedSession {
    func setUp() {}
}
