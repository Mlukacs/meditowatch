//
//  HealthKitService.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 08/12/2020.
//

import Foundation
import Combine
import HealthKit

struct HealtKitData {
    let totalMinutes: Int
    let numberOfEvents: Int
    let averageTimeSession: Double
}

enum HealtKitTimeSpand {
    case week
    case month
    case year
}

protocol HealthKitServiceContract {
    var healthKitData: PassthroughSubject<HealtKitData, Never> { get }
    func startMeditation()
    func stopMeditation()
    func askPermission()
    func getMindfullData(forPast time: HealtKitTimeSpand)
}

final class HealthKitService: HealthKitServiceContract {
    var healthKitData: PassthroughSubject<HealtKitData, Never> = .init()
    
    private let healthStore = HKHealthStore()
    private var beginningOfSession: Date?
    
    init() {}
    
    func askPermission() {
        guard let healtKitType = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.mindfulSession) else {
            return
        }
        
        let setHealtKitObject = Set([healtKitType])
        
        let typestoRead = setHealtKitObject
        let typestoShare = setHealtKitObject
        
        self.healthStore.requestAuthorization(toShare: typestoShare, read: typestoRead) { (success, error) -> Void in
            if success == false {
                print("solve this error\(String(describing: error))")
                NSLog(" Display not allowed")
            }
            if success == true {
                print("dont worry everything is good\(success)")
                NSLog(" Integrated SuccessFully")
            }
        }
    }
    
    func startMeditation() {
        beginningOfSession = Date()
    }
    
    func stopMeditation() {
        saveMindfullAnalysis()
    }
    
    func getMindfullData(forPast time: HealtKitTimeSpand) {
        // Getting quantityType as stepCount
        guard let mindfulType = HKObjectType.categoryType(forIdentifier: .mindfulSession) else {
            print("*** Unable to create a step count type ***")
            return
        }
        
        let now = Date()
        let startDate: Date?
        switch time {
        case .week:
            startDate = Calendar.current.date(byAdding: DateComponents(day: -7), to: now)
        case .month:
            startDate = Calendar.current.date(byAdding: DateComponents(month: -1), to: now)
        case .year:
            startDate = Calendar.current.date(byAdding: DateComponents(year: -1), to: now)
        }
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: now, options: .strictStartDate)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        
        let query = HKSampleQuery(
            sampleType: mindfulType,
            predicate: predicate,
            limit: 0,
            sortDescriptors: [sortDescriptor],
            resultsHandler: updateMeditationTime
        )
        
        healthStore.execute(query)
    }
    
    func updateMeditationTime(query: HKSampleQuery, results: [HKSample]?, error: Error?) {
        guard let results = results, error == nil  else {
            return
        }
        
        // Sum the meditation time
        let totalMeditationTime = results.map(calculateTotalTime).reduce(0, { $0 + $1 })
        renderMeditationMinuteText(totalMeditationSeconds: totalMeditationTime, numberOfEvents: results.count)
    }
    
    func calculateTotalTime(sample: HKSample) -> TimeInterval {
        let totalTime = sample.endDate.timeIntervalSince(sample.startDate)
        return totalTime
    }
    
    func renderMeditationMinuteText(totalMeditationSeconds: Double, numberOfEvents: Int) {
        let minutes = Int(totalMeditationSeconds / 60)
        let average = Double(minutes) / Double(numberOfEvents)
        let data = HealtKitData(totalMinutes: minutes, numberOfEvents: numberOfEvents, averageTimeSession: average)
        healthKitData.send(data)
    }
}

private extension HealthKitService {
    func saveMindfullAnalysis() {
        guard let mindfulType = HKObjectType.categoryType(forIdentifier: .mindfulSession),
              let beginningOfSession = beginningOfSession else {
            return
        }
        let endTime = Date()
        let mindfullSession = HKCategorySample(type: mindfulType, value: 0, start: beginningOfSession, end: endTime)
        
        healthStore.save(mindfullSession) { [weak self] success, error -> Void in
            self?.beginningOfSession = nil
            if error != nil {
                // something happened
                return
            }
            
            if success {
                print("My new data was saved in HealthKit")
            } else {
                // something happened again
            }
        }
    }
}
