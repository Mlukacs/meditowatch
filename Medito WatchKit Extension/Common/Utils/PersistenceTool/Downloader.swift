//
//  Downloader.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 02/01/2021.
//

import Foundation
import AVFoundation
import Combine
import WatchKit
import Resolver

enum DownloadStatus: Equatable {
    case idle
    case downloading
    case finished
}

typealias PlayableStatus = (data: AudioData?, status: DownloadStatus)

protocol PersistenceContract {
    var downloadInformation: CurrentValueSubject<PlayableStatus, Never> { get }
    var downloadProgression: PassthroughSubject<Float, Never> { get }
    func cancelDownload()
    func deleteLocalData(for audio: AudioData)
    func download(for audio: AudioData)
}

final class Downloader: NSObject, PersistenceContract {
    private var urlSession: URLSession!
    private var didRestorePersistenceManager: Bool = false
    private var activeDownloadsMap = [URLSessionDownloadTask: File]()
    private var downloadQueue = [File]()
    private var currentAudioData: AudioData?
    private var newFiles = [File]()
    private var totalDownloads: Int = 0
    private var resumeData: [String: Data]?
    @Injected private var extendedSession: ExtendedSessionContract
    
    var downloadInformation: CurrentValueSubject<PlayableStatus, Never> = .init((data: nil, status: .idle))
    var downloadProgression: PassthroughSubject<Float, Never> = .init()
    
    override init() {
        super.init()
        
        self.urlSession = createURLSession()
        createDirectory(to: getMeditoDirectoryPath())
    }
    
    private func createURLSession() -> URLSession {
        let config = URLSessionConfiguration.background(withIdentifier: "WatchDownloader")
        config.waitsForConnectivity = true
        config.sessionSendsLaunchEvents = true
        config.httpMaximumConnectionsPerHost = 2
        config.timeoutIntervalForResource = 3600
        config.isDiscretionary = false
        config.allowsExpensiveNetworkAccess = true
        config.allowsCellularAccess = true
        config.allowsConstrainedNetworkAccess = false
        config.shouldUseExtendedBackgroundIdleMode = true
        
        return URLSession(configuration: config, delegate: self, delegateQueue: nil)
    }
    
    func getMeditoDirectoryPath() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).map(\.path)
        let path = URL(fileURLWithPath: paths[0]).appendingPathComponent("Medito")
        
        return path
    }
    
    func createDirectory(to url: URL) {
        guard !FileManager.default.fileExists(atPath: url.path) else {
            return
        }
        
        do {
            try FileManager.default.createDirectory(
                atPath: url.path,
                withIntermediateDirectories: true,
                attributes: nil)
        } catch let error {
            print("Create directory error: \(error)")
        }
    }
    
    func cancelDownload() {
        guard let cancelledCurrentAudioData = currentAudioData else {
            return
        }
        var task: URLSessionDownloadTask?
        let documentsPath = getMeditoDirectoryPath().appendingPathComponent(cancelledCurrentAudioData.id)
        removeFile(at: documentsPath)
        downloadQueue.removeAll()
        currentAudioData = nil
        newFiles.removeAll()
        
        for (taskKey, audioItem) in activeDownloadsMap where cancelledCurrentAudioData.id == audioItem.id {
            task = taskKey
            break
        }
        
        task?.cancel()
        extendedSession.stop()
        let newStatus = (data: cancelledCurrentAudioData, status: DownloadStatus.idle)
        downloadInformation.send(newStatus)
    }
    
    func removeFile(at url: URL) {
        do {
            try FileManager.default.removeItem(at: url)
        } catch {
            print("Error: could not find and delete decoded media element")
        }
    }
    
    func removeFile(for path: String) {
        do {
            try FileManager.default.removeItem(atPath: path)
        } catch {
            print("Error: could not find and delete decoded media element")
        }
    }
    
    func removeAll() {
        let directoryPath = getMeditoDirectoryPath()
        do {
            let fileManager = FileManager.default
            
            // Check if file exists
            if fileManager.fileExists(atPath: directoryPath.path) {
                // Delete file
                try fileManager.removeItem(atPath: directoryPath.path)
            } else {
                print("Folder does not exist")
            }
            createDirectory(to: directoryPath)
        } catch {
            print("An error took place: \(error)")
        }
    }
    
    func deleteLocalData(for audio: AudioData) {
        removeFile(at: getMeditoDirectoryPath().appendingPathComponent(audio.id))
    }
    
    func download(for audioData: AudioData) {
        guard !audioData.content.files.isEmpty else { return }
        let documentsPath = getMeditoDirectoryPath().appendingPathComponent(audioData.id)
        createDirectory(to: documentsPath)
        currentAudioData = audioData
        downloadQueue = audioData.content.files
        totalDownloads = audioData.content.files.count
        let newStatus = (data: audioData, status: DownloadStatus.downloading)
        downloadInformation.send(newStatus)
        
        if let file = downloadQueue.first {
            download(file)
            downloadQueue.remove(at: 0)
        }
    }
    
    private func download(_ file: File) {
        guard let contentURL = file.getUrl() else { return }
        extendedSession.start()
        let task = urlSession.downloadTask(with: contentURL)
        task.taskDescription = file.id
        activeDownloadsMap[task] = file
        
        task.resume()
    }
    
    private func downloadNext() {
        guard resumeData == nil else {
            //            resumeDownload()
            return
        }
        guard let file = downloadQueue.first else {
            return
        }
        download(file)
        downloadQueue.remove(at: 0)
    }
    
    func hasContentToDownload() -> Bool {
        downloadQueue.first != nil
    }
}

extension Downloader: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64,
                    totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64) {
        
        let percentOfEachFile: Double = Double(100 / totalDownloads)
        let alreadyDownloaded = percentOfEachFile * Double(newFiles.count)
        if let file = self.activeDownloadsMap[downloadTask],
           totalBytesExpectedToWrite > 0 {
            var progress = (Double(totalBytesWritten) / Double(totalBytesExpectedToWrite)) * percentOfEachFile
            progress += alreadyDownloaded
            print("progress of file id: \(file.id): \(progress.rounded())%")
            downloadProgression.send(Float(progress.rounded()))
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL) {
        
        // 1
        guard let downloadedFile = activeDownloadsMap.removeValue(forKey: downloadTask),
              let audioData = currentAudioData else {
            _ = try? FileManager.default.removeItem(atPath: location.path)
            return
        }
        let documentsPath = getMeditoDirectoryPath().appendingPathComponent( audioData.id)
        // 2
        let destinationURL = documentsPath.appendingPathComponent("\(downloadedFile.getCleanId())")
        
        // 3
        let fileManager = FileManager.default
        
        do {
            try fileManager.copyItem(at: location, to: destinationURL)
            fillNewFiles(downloadedFile.id, newPath: destinationURL.absoluteString)
            if !hasContentToDownload() {
                guard let currentAudioData = currentAudioData else {
                    return
                }
                let newAudio = currentAudioData.copy(with: newFiles, isLocal: true)
                let newStatus = (data: newAudio, status: DownloadStatus.finished)
                downloadInformation.send(newStatus)
                cleanDownloader()
            } else {
                downloadNext()
            }
        } catch let error {
            print("Could not copy file to disk: \(error.localizedDescription)")
            cancelDownload()
        }
    }
    
    func fillNewFiles(_ fileId: String, newPath: String) {
        guard let oldFile = currentAudioData?.content.files.first(where: { $0.id == fileId}) else {
            return
        }
        let file = File(id: oldFile.id, type: oldFile.type, url: newPath, info: oldFile.info)
        newFiles.append(file)
    }
    
    func cleanDownloader() {
        guard let testCurrentAudioData = currentAudioData else {
            return
        }
        extendedSession.stop()
        let newStatus = (data: testCurrentAudioData, status: DownloadStatus.idle)
        downloadInformation.send(newStatus)
        downloadProgression.send(0)
        currentAudioData = nil
        newFiles.removeAll()
    }
}
