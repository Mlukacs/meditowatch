//
//  MainRouter.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 01/12/2020.
//

import SwiftUI
import Combine

final class MainRouter {
    var shouldPresentPlayerOverlay: CurrentValueSubject<Bool, Never> = .init(false)
    var shouldPresentDeepLink: PassthroughSubject<Bool, Never> = .init()
    var deeplinkDestinationType: ReminderType = .daily
}

protocol OverlayPresentingContract {
    var shouldPresentPlayerOverlay: CurrentValueSubject<Bool, Never> { get }
    var shouldPresentDeepLink: PassthroughSubject<Bool, Never> { get set }
    func navigateToDeeplinkType(for type: String)
}

protocol RouteToPageContract: OverlayPresentingContract {
    func routeToPage(for pack: MindfullPack) -> AnyView
    func routeToAudioPage(for audio: AudioData, isOffline: Bool) -> AnyView
    func routeToPlayer(with audioData: AudioData?,
                       audioFile: File?,
                       and backgroundSound: BackgroundSound?) -> AnyView
    func routeToPlayer() -> AnyView
    func deepLink() -> AnyView
}

protocol RouteToUserPageContract: OverlayPresentingContract {
    func routeToPage(for tile: UserContentTile) -> AnyView
    func routeToPlayer() -> AnyView
}

extension MainRouter: RouteToPageContract {
    func routeToPage(for pack: MindfullPack) -> AnyView {
        switch pack.type {
        case "folder":
            return folderPage(with: pack.getContentPath())
        case "session", "daily":
            return audioPage(with: pack.getContentPath())
        case "article", "text":
            return textPage(with: pack.getContentPath())
        default:
            return Text("don't know this type of context").eraseToAnyView()
        }
    }
    
    func navigateToDeeplinkType(for type: String) {
        guard let destination = ReminderType(rawValue: type) else {
            return
        }
        deeplinkDestinationType = destination
        shouldPresentDeepLink.send(true)
    }
    
    func routeToAudioPage(for audio: AudioData, isOffline: Bool) -> AnyView {
        let audioPage = AudioPageView()
        audioPage.viewModel.setUpViewModel(with: audio)
        audioPage.viewModel.shouldDismiss = isOffline
        return audioPage.eraseToAnyView()
    }

    func routeToPlayer(with audioData: AudioData?, audioFile: File?, and backgroundSound: BackgroundSound?) -> AnyView {
        let playerView = PlayerPageView()
        playerView.viewModel.setUpViewModel(with: audioData, audioFile: audioFile, backgroundSound: backgroundSound)
        return playerView.eraseToAnyView()
    }
    
    func routeToPlayer() -> AnyView {
        let playerView = PlayerPageView()
        return playerView.eraseToAnyView()
    }
    
    func deepLink() -> AnyView {
        shouldPresentDeepLink.send(false)
        switch deeplinkDestinationType {
        case .daily:
            let path = APIConfig.dailyURL
            return audioPage(with: path)
        case .timer:
            let path = APIConfig.timerURL
            return audioPage(with: path)
        case .sleep:
            let path = APIConfig.sleepURL
            return folderPage(with: path)
        case .sound:
            let path = APIConfig.soundsURL
            return folderPage(with: path)
        }
    }
}

extension MainRouter: RouteToUserPageContract {
    func routeToPage(for tile: UserContentTile) -> AnyView {
        switch tile.type {
        case .favorites:
            return FavoritesView().eraseToAnyView()
        case .offline:
            return OfflineView().eraseToAnyView()
        case .dataTracking:
            return StatisticView().eraseToAnyView()
        case .reminder:
            return ReminderView().eraseToAnyView()
        default:
            return EmptyView().eraseToAnyView()
        }
    }
}

private extension MainRouter {
    func audioPage(with path: String) -> AnyView {
        let audioPage = AudioPageView()
        audioPage.viewModel.setUpViewModel(with: path)
        return audioPage.eraseToAnyView()
    }
    
    func textPage(with path: String) -> AnyView {
        let textPage = TextPageView()
        textPage.viewModel.setUpViewModel(with: path)
        return textPage.eraseToAnyView()
    }
    
    func folderPage(with path: String) -> AnyView {
        let folderPage = FolderPageView()
        folderPage.viewModel.setUpViewModel(for: path)
        return folderPage.eraseToAnyView()
    }
}
