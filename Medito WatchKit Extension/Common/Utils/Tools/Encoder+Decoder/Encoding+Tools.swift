//
//  Encoding+Tools.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 05/12/2020.
//

import Foundation

// MARK: - Helper functions for creating encoders and decoders

func watchJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    decoder.dateDecodingStrategy = .iso8601
    return decoder
}

func watchJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    encoder.dateEncodingStrategy = .iso8601
    return encoder
}
