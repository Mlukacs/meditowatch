//
//  AudioPageView.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 02/12/2020.
//

import SwiftUI
import Kingfisher
import KingfisherSwiftUI
import UIKit
import Resolver

struct AudioPageView: View {
    @InjectedObject var viewModel: AudioPageViewModel
    @Environment(\.presentationMode) private var presentationMode
    @State private var alreadyLoaded: Bool = false
    @State private var navigationActivated: Bool = false

    var body: some View {
        ZStack {
            navigationView
            containerView
        }.navigationBarTitle(Text(viewModel.audioData?.title ?? ""))
        .onAppear(perform: {
            navigationActivated = false
            if !alreadyLoaded {
                alreadyLoaded = true
                viewModel.fetchContent()
            }
        })
    }
}

// MARK: Main container view
private extension AudioPageView {
    var containerView : some View {
        Group {
            if viewModel.isLoading {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .white))
            } else if viewModel.audioData == nil {
                emptyView
            } else {
                ZStack(alignment: .bottomTrailing) {
                    audioDisplayView
                    playButton
                }.edgesIgnoringSafeArea(.bottom)
            }
        }
    }
}

private extension AudioPageView {
    var navigationView : some View {
        NavigationLink(destination:
                        LazyView(viewModel.router.routeToPlayer(with: viewModel.audioData,
                                                                audioFile: viewModel.selectedFile,
                                                                and: viewModel.selectedSound)),
                       isActive: $navigationActivated) {
            EmptyView().frame(width: 0, height: 0)
        }.buttonStyle(PlainButtonStyle())
        .hidden()
        .onAppear(perform: {
            navigationActivated = false
        })

    }
}

private extension AudioPageView {
    var emptyView : some View {
        VStack {
            Text("No Content to display. Try and refresh the page.")
            Button(action: {
                self.viewModel.fetchContent()
            }) {
                Spacer()
                Text("Refresh")
                    .padding()
                Spacer()
            }.buttonStyle(PlainButtonStyle())
            .frame(height: 45)
            .background(Color.defaultMainCoral)
            .cornerRadius(15)
        }
    }
}

private extension AudioPageView {
    var audioDisplayView : some View {
        ScrollView(showsIndicators: false) {
            VStack {
                topView
                
                if !viewModel.voices.isEmpty {
                    Divider()
                        .padding(.top, 10)
                        .padding(.bottom, 5)
                    voiceDisplay
                }
                
                if !viewModel.voiceAudioFiles.isEmpty {
                    Divider()
                        .padding(.top, 10)
                        .padding(.bottom, 5)
                    durationDisplay
                }
                
                if viewModel.hasBackgroundMusic, let data = viewModel.audioData, !data.isLocal {
                    Divider()
                        .padding(.top, 10)
                        .padding(.bottom, 5)
                    backGroundSoundsDisplay
                }
                
                Rectangle()
                    .fill(Color.clear)
                    .frame(height: 55)
            }
        }
    }
}

// MARK: - Cover Image
private extension AudioPageView {
    var topView : some View {
        Group {
            HStack {
                coverImage
                Spacer()
                VStack {
                    Spacer()
                    HStack {
                        if viewModel.downloadingStatus == .idle {
                            viewModel.isLocal ? removeButton.eraseToAnyView() : downloadButton.eraseToAnyView()
                        } else if viewModel.downloadingStatus == .downloading {
                            progressionButton
                        }
                        if let data = viewModel.audioData, !data.isLocal {
                            favButton
                        }
                    }
                }
            }.padding(.bottom, 10)
            .padding(.top, 10)
            .background(LinearGradient(gradient:
                                        Gradient(colors: [Color(hex: viewModel.audioData?.content.getPrimaryColor() ?? ""), .black]),
                                       startPoint: .top,
                                       endPoint: .bottom))
            VStack(alignment: .leading, spacing: 5) {
                Text(viewModel.audioData?.title ?? "")
                    .fontWeight(.semibold)
                    .font(.system(size: 15))
                    .lineLimit(2)
                    .foregroundColor(Color.lightTextColor)
                
                Text(viewModel.description)
                    .fontWeight(.medium)
                    .font(.system(size: 12))
                    .foregroundColor(Color.lightTextColor)
            }
        }
    }
}

// MARK: - Cover Image
private extension AudioPageView {
    var voiceDisplay : some View {
        VStack(alignment: .leading, spacing: 5) {
            Text("Narrators")
                .fontWeight(.light)
                .font(.system(size: 11))
                .foregroundColor(Color.lightTextColor)
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 5) {
                    ForEach(viewModel.voices, id: \.self) { voice in
                        Button(action: { self.viewModel.setSelectedVoice(for: voice)}) {
                            Text(voice)
                                .padding(.leading, 10)
                                .padding(.trailing, 10)
                                .padding(.top, 5)
                                .padding(.bottom, 5)
                                .background(viewModel.isSelectedVoice(for: voice) ? Color.lightTextColor : Color.darkBGColor)
                                .cornerRadius(10)
                        }.buttonStyle(PlainButtonStyle())
                    }
                }
            }
        }
    }
}

// MARK: - Cover Image
private extension AudioPageView {
    var durationDisplay : some View {
        VStack(alignment: .leading, spacing: 5) {
            Text("Session length")
                .fontWeight(.light)
                .font(.system(size: 11))
                .foregroundColor(Color.lightTextColor)
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 5) {
                    ForEach(viewModel.voiceAudioFiles) { file in
                        Button(action: { viewModel.setSelectedFile(for: file)}) {
                            Text("\(file.getLength()) mins")
                                .padding(.leading, 10)
                                .padding(.trailing, 10)
                                .padding(.top, 5)
                                .padding(.bottom, 5)
                                .background(viewModel.isSelectedFile(for: file) ? Color.lightTextColor : Color.darkBGColor)
                                .cornerRadius(10)
                        }.buttonStyle(PlainButtonStyle())
                    }
                }
            }
        }
    }
}

// MARK: - Cover Image
private extension AudioPageView {
    var backGroundSoundsDisplay : some View {
        VStack(alignment: .leading, spacing: 5) {
            Text("Background Sounds")
                .fontWeight(.light)
                .font(.system(size: 11))
                .foregroundColor(Color.lightTextColor)
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 5) {
                    ForEach(viewModel.sounds, id: \.self) { sound in
                        Button(action: { self.viewModel.setSelectedSound(for: sound)}) {
                            Text(sound)
                                .padding(.leading, 10)
                                .padding(.trailing, 10)
                                .padding(.top, 5)
                                .padding(.bottom, 5)
                                .background(viewModel.isSelectedSound(for: sound) ? Color.lightTextColor : Color.darkBGColor)
                                .cornerRadius(10)
                        }.buttonStyle(PlainButtonStyle())
                    }
                }
            }
        }
    }
}

private extension AudioPageView {
    var playButton : some View {
        Button(action: {
            navigationActivated = true
        }) {
            HStack {
                Spacer()
                ZStack(alignment: .center) {
                    Image("blurb")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundColor(Color(hex: viewModel.audioData?.content.getPrimaryColor() ?? ""))
                        .frame(width: 40, height: 40)
                    Image(systemName: "play.fill")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundColor(.black)
                        .frame(width: 15, height: 15)
                }
            }
        }.buttonStyle(PlainButtonStyle())
        .padding()
    }
}

private extension AudioPageView {
    var downloadButton: some View {
        Button(action: { viewModel.startDownload() }) {
            Image(systemName: "arrow.down.circle")
                .resizable()
                .renderingMode(.template)
                .foregroundColor(.white)
                .frame(width: 25, height: 25)
        }.buttonStyle(PlainButtonStyle())
        .disabled(!viewModel.canDownload)
        .padding()
    }
}

private extension AudioPageView {
    var progressionButton: some View {
        Button(action: { viewModel.cancelDownload() }) {
            ZStack {
                Text("\(Int(viewModel.progression))%")
                    .minimumScaleFactor(0.3)
                    .frame(width: 23, height: 23)
                DownloadCircularProgressBar(progress: (viewModel.progression / 100))
            }.frame(width: 25, height: 25)
        }.buttonStyle(PlainButtonStyle())
        .padding()
    }
}

private struct DownloadCircularProgressBar: View {
    var progress: Float
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(lineWidth: 1.0)
                .opacity(0.3)
                .foregroundColor(.white)
            
            Circle()
                .trim(from: 0.0, to: CGFloat(min(self.progress, 1.0)))
                .stroke(style: StrokeStyle(lineWidth: 1.0, lineCap: .round, lineJoin: .round))
                .foregroundColor(.green)
                .rotationEffect(Angle(degrees: 90.0))
        }
    }
}

private extension AudioPageView {
    var removeButton: some View {
        Button(action: {
            remove()
        }) {
            Image(systemName: "xmark.bin.circle")
                .resizable()
                .renderingMode(.template)
                .foregroundColor(.white)
                .frame(width: 25, height: 25)
        }.buttonStyle(PlainButtonStyle())
        .padding()
    }
    
    func remove() {
        viewModel.removeDownload()
        if viewModel.shouldDismiss {
            self.presentationMode.wrappedValue.dismiss()
        }
    }
}

private extension AudioPageView {
    var favButton: some View {
        Button(action: {viewModel.toggleFavorite()}) {
            Image(systemName: viewModel.isInFav ? "heart.fill"  : "heart")
                .resizable()
                .renderingMode(.template)
                .foregroundColor(viewModel.isInFav ? .red : .white)
                .frame(width: 25, height: 25)
        }.buttonStyle(PlainButtonStyle())
        .padding()
    }
}

// MARK: - Cover Image
private extension AudioPageView {
    var coverImage : some View {
        KFImage(URL(string: viewModel.audioData?.content.cover.first?.url ?? ""),
                options: [
                    .processor(
                        DownsamplingImageProcessor(size: CGSize(width: 100, height: 100))
                    ),
                    .cacheOriginalImage
                ])
            .cancelOnDisappear(true)
            .resizable()
            .placeholder {
                Image("icon_ic_logo")
            }
            .frame(width: 50, height: 50)
            .aspectRatio(contentMode: .fit)
            .background(Color(hex: viewModel.audioData?.content.getPrimaryColor() ?? ""))
            .cornerRadius(5.0)
            .padding(.leading, 5)
    }
}

struct AudioPageView_Previews: PreviewProvider {
    static var previews: some View {
        AudioPageView()
    }
}
