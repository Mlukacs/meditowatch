//
//  AudioPageView.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 02/12/2020.
//

import Foundation
import Combine
import SwiftUICombineToolBox
import Resolver

final class AudioPageViewModel: ObservableObject {
    
    @Published var voices: [String] = []
    @Published var sounds: [String] = []
    @Published var voiceAudioFiles: [File] = []
    @Published var durations: [String] = []
    @Published var hasBackgroundMusic: Bool = false
    @Published var isLoading = false
    @Published var isInFav = false
    @Published var isLocal = false
    @Published var canDownload = true
    @Published var selectedFile: File?
    @Published var selectedSoundName: String = ""
    @Published var description: String = ""
    @Published var audioData: AudioData?
    @Published var downloadingStatus: DownloadStatus = .idle
    @Published var progression: Float = 0
    @Published var shouldDismiss: Bool = false
    
    private var selectedVoice: String = ""
    private var voiceFiles: [String: [File]] = [:]
    private var path: String?
    var selectedSound: BackgroundSound?
    @Injected private var audioRepository: AudioDataRepositoryContract
    @Injected private var backgroundSoundsRepository: BackgroundSoundsRepositoryContract
    @Injected private var favoriteRepository: FavoritesRepositoryContract
    @Injected private var localRepository: LocalRepositoryContract
    @Injected private var healthKitService: HealthKitServiceContract
    @Injected var router: RouteToPageContract
    private var cancelBag = CancelBag()
    
    init() {
        setUp()
    }
    
    func setUpViewModel(with path: String) {
        self.path = path
    }
    
    func setUpViewModel(with audioData: AudioData) {
        self.audioData = audioData
        self.path = nil
        setUpView()
    }
    
    func askHealthKitPermission() {
        healthKitService.askPermission()
    }
    
    func clear() {
        audioData = nil
    }
}

// MARK: - Actions
extension AudioPageViewModel {
    
    func setSelectedVoice(for voice: String) {
        selectedVoice = voice
        voiceAudioFiles = voiceFiles[selectedVoice] ?? []
        
        guard let validSelectedFile = selectedFile else {
            selectedFile = voiceAudioFiles.first
            return
        }
        if let (index, _) = voiceAudioFiles.map({ abs($0.getLength() - validSelectedFile.getLength()) }).enumerated().min(by: { $0.1 < $1.1 }) {
            selectedFile = voiceAudioFiles[index]
        }
    }
    
    func isSelectedVoice(for voice: String) -> Bool {
        return voice == selectedVoice
    }
    
    func setSelectedFile(for file: File) {
        selectedFile = file
    }
    
    func isSelectedFile(for file: File) -> Bool {
        guard let selectedFile = selectedFile else {
            return false
        }
        return selectedFile.id == file.id
    }
    
    func setSelectedSound(for sound: String) {
        selectedSoundName = sound
        selectedSound = backgroundSoundsRepository.sound(for: sound)
    }
    
    func isSelectedSound(for sound: String) -> Bool {
        return selectedSoundName == sound
    }
}

// MARK: - Favorite Actions
extension AudioPageViewModel {
    func toggleFavorite() {
        guard let audioData = audioData else {
            return
        }
        if isInFav {
            favoriteRepository.removeFromFavorite(for: audioData)
        } else {
            favoriteRepository.addToFavorite(for: audioData)
        }
        
        isInFav.toggle()
    }
}

// MARK: - Download Actions

extension AudioPageViewModel {
    func startDownload() {
        guard let audioData = audioData else {
            return
        }
        localRepository.addToLocalData(for: audioData)
    }
    
    func cancelDownload() {
        localRepository.cancelLocalPersistance()
    }
    
    func removeDownload() {
        guard let audioData = audioData else {
            return
        }
        isLocal = false
        localRepository.removeFromLocalData(for: audioData)
    }
}

// MARK: - Data fetching

extension AudioPageViewModel {
    func fetchContent() {
        guard let path = path else {
            return
        }
        
        isLoading = true
        audioRepository.audioData(for: path)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: {[weak self] completion in
                switch completion {
                case .finished:
                    self?.isLoading = false
                case .failure(let error):
                    self?.isLoading = false
                    switch error {
                    case let decodingError as DecodingError:
                        print("\(decodingError.localizedDescription)")
                    case let networkingError as NetworkingError:
                        print("\(networkingError.localizedDescription)")
                    default:
                        print("\(error.localizedDescription)")
                    }
                }
            }) { [weak self] audioData in
                guard let self = self else {
                    return
                }
                self.isLoading = false
                self.audioData = audioData
                self.description = audioData.content.getDescription()
                self.setUpView()
                self.askHealthKitPermission()
            }.store(in: &cancelBag)
    }
}

private extension AudioPageViewModel {
    func setUp() {
        backgroundSoundsRepository.backgroundSoundsTitles
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] backgroundSounds in
                self?.sounds = backgroundSounds
                self?.selectedSoundName = self?.sounds.first ?? ""
            })
            .store(in: &cancelBag)
        
        localRepository.downloadInformation
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] playableStatus in
                guard let audioData = self?.audioData,
                      let data = playableStatus.data,
                      data.id == audioData.id
                      else {
                    self?.downloadingStatus = .idle
                    return
                }
                self?.downloadingStatus = playableStatus.status
                if playableStatus.status == .finished {
                    self?.isLocal = true
                }
            })
            .store(in: &cancelBag)

        localRepository.downloadInformation
            .receive(on: DispatchQueue.main)
            .map { playableStatus in
                playableStatus.status == .idle
            }.assignNoRetain(to: \.canDownload, on: self)
            .store(in: &cancelBag)

        Publishers.CombineLatest3(localRepository.downloadInformation, localRepository.downloadProgression, $audioData)
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] downloadInformation, progression, audioData in
                guard let audioData = audioData,
                      let currentDownload = downloadInformation.data,
                      audioData.id == currentDownload.id,
                      downloadInformation.status == .downloading
                else {
                    self?.downloadingStatus = .idle
                    return
                }
                self?.downloadingStatus = .downloading
                self?.progression = progression
            })
            .store(in: &cancelBag)
        
    }
    
    func setUpView() {
        guard let audioData = audioData else {
            return
        }
        hasBackgroundMusic = audioData.content.hasBackgroundMusic
        voiceFiles = audioData.content.getOrderedFiles()
        fillUpDisplayedData()
        isInFav = favoriteRepository.isInFavorites(for: audioData)
        isLocal = localRepository.isOnDevice(for: audioData)
    }
    
    func fillUpDisplayedData() {
        var newVoices: [String] = []
        for (key, _) in voiceFiles {
            newVoices.append(key)
        }
        voices = newVoices.sorted()
        selectedVoice = voices.first ?? ""
        voiceAudioFiles = voiceFiles[selectedVoice] ?? []
        selectedFile = voiceAudioFiles.first
    }
}
