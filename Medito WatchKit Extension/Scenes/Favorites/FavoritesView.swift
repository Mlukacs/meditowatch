//
//  FavoritesView.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 06/12/2020.
//

import SwiftUI
import Resolver

struct FavoritesView: View {
    @InjectedObject private var viewModel: FavoritesViewModel
    
    var body: some View {
        containerView
            .navigationBarTitle(Text("Favorites"))
    }
}

// MARK: Main container view
private extension FavoritesView {
    var containerView : some View {
        Group {
            if viewModel.favorites.isEmpty {
                emptyView
            } else {
                fullFavoritesView
            }
        }
    }
}

private extension FavoritesView {
    var fullFavoritesView : some View {
        Group {
            List {
                ForEach(viewModel.favorites) { favorite in
                    NavigationLink(destination:
                                    LazyView(viewModel.router.routeToAudioPage(for: favorite, isOffline: false))
                    ) {
                        AudioDataRow(data: favorite)
                    }
                }
                .listRowInsets(EdgeInsets())
            }.listStyle(CarouselListStyle())
        }
    }
}

private extension FavoritesView {
    var emptyView : some View {
        VStack {
            Text("It's a bit empty here. You should add content to your favorites.")
        }
    }
}

struct FavoritesView_Previews: PreviewProvider {
    static var previews: some View {
        FavoritesView()
    }
}
