//
//  FavoritesViewModel.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 06/12/2020.
//

import Foundation
import Combine
import SwiftUICombineToolBox
import Resolver

final class FavoritesViewModel: ObservableObject {

    @Published var favorites: [AudioData] = []
    @Published var hasContentLoaded: Bool = false
    
    @Injected private var favoriteRepository: FavoritesRepositoryContract
    @Injected var router: RouteToPageContract
    private var cancelBag = CancelBag()
    
    init() {
        setUp()
    }
}

private extension FavoritesViewModel {
    func setUp() {
        favoriteRepository.favorites
            .receive(on: DispatchQueue.main)
            .assignNoRetain(to: \.favorites, on: self)
            .store(in: &cancelBag)
    }
}
