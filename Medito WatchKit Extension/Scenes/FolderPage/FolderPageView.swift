//
//  FolderPageView.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 31/01/2021.
//

import SwiftUI
import Resolver

struct FolderPageView: View {
    @InjectedObject var viewModel: FolderPageViewModel
    
    init() {}
    
    var body: some View {
        containerView
            .navigationBarTitle(Text(viewModel.title))
            .onAppear(perform: { viewModel.fetchContent() })
    }
}

// MARK: Main container view
private extension FolderPageView {
    var containerView : some View {
        Group {
            if viewModel.isLoading {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .white))
            } else if viewModel.packs.isEmpty {
                emptyView
            } else {
                fullListView
            }
        }
    }
}

private extension FolderPageView {
    var emptyView : some View {
        VStack {
            Text("No Content to display. Try and refresh the page.")
            Button(action: {
                self.viewModel.fetchContent()
            }) {
                Spacer()
                Text("Refresh")
                    .padding()
                Spacer()
            }.buttonStyle(PlainButtonStyle())
            .frame(height: 45)
            .background(Color.defaultMainCoral)
            .cornerRadius(15)
        }
    }
}

private extension FolderPageView {
    var fullListView : some View {
        Group {
            List {
                ForEach(viewModel.packs) { pack in
                    NavigationLink(destination:
                                    LazyView(viewModel.router.routeToPage(for: pack))
                    ) {
                        FolderPageRow(pack: pack)
                    }.eraseToAnyView()
                }
                .listRowBackground(Color.clear)
                .listRowInsets(EdgeInsets())
            }.listStyle(CarouselListStyle())
        }
    }
}

struct FolderPageView_Previews: PreviewProvider {
    static var previews: some View {
       FolderPageView()
    }
}
