//
//  FolderPageViewModel.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 31/01/2021.
//

import Foundation
import Combine
import SwiftUICombineToolBox
import Resolver

final class FolderPageViewModel: ObservableObject {

    @Published var packs: [MindfullPack] = []
    @Published var isLoading = false
    @Published var title = ""
    @Published var pack: MindfullPack?
    private var path: String?

    @Published var hasContentLoaded: Bool = false

    @Injected private var repository: MindfullPackRepositoryContract

    @Injected var router: RouteToPageContract
    private var cancelBag = CancelBag()
    
    init() {}
    
    func setUpViewModel(for path: String) {
        self.path = path
    }
    
    func fetchContent() {
        guard let path = path else {
            return
        }
        isLoading = true
        repository.packs(for: path)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: {[weak self] completion in
                switch completion {
                case .finished:
                    self?.isLoading = false
                case .failure(let error):
                    self?.isLoading = false
                    switch error {
                    case let decodingError as DecodingError:
                        print("\(decodingError.localizedDescription)")
                    case let networkingError as NetworkingError:
                        print("\(networkingError.localizedDescription)")
                    default:
                        print("\(error.localizedDescription)")
                    }
                }
            }) { [weak self] packContainer in
                self?.isLoading = false
                self?.packs = packContainer.getPacks()
                self?.title = packContainer.data.content.title
            }.store(in: &cancelBag)
    }
}
