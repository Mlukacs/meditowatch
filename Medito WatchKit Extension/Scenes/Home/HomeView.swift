//
//  ContentView.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 30/11/2020.
//

import SwiftUI
import Resolver
import SwiftUICombineToolBox

struct HomeView: View {
    @InjectedObject private var viewModel: HomeViewModel
    
    var body: some View {
        containerView
            .navigationTitle {
                NavigationBarLogo(iconeName: "icon_ic_logo")
            }
            .onAppear {
                viewModel.fetchContent()
            }
    }
}

// MARK: Main container view
private extension HomeView {
    var containerView : some View {
        Group {
            if viewModel.isLoading {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .white))
            } else if viewModel.homePacks.isEmpty {
                emptyView
            } else {
                fullTilesView
            }
        }
    }
}

private extension HomeView {
    var emptyView : some View {
        VStack {
            Text("No Content to display. Try and refresh the page.")
            Button(action: {
                viewModel.fetchContent()
            }) {
                Spacer()
                Text("Refresh")
                    .padding()
                Spacer()
            }.buttonStyle(PlainButtonStyle())
            .frame(height: 45)
            .background(Color.defaultMainCoral)
            .cornerRadius(15)
        }
    }
}

private extension HomeView {
    var fullTilesView : some View {
        Group {
            List {
                ForEach(viewModel.homePacks) { pack in
                    NavigationLink(destination:
                                    viewModel.router.routeToPage(for: pack).embedInLazyView()
                    ) {
                        PackRow(pack: pack)
                    }
                }
                .listRowBackground(Color.clear)
                .listRowInsets(EdgeInsets())
            }.listStyle(CarouselListStyle())
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
