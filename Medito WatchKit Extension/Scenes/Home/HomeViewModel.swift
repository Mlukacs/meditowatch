//
//  HomeViewModel.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 30/11/2020.
//

import Foundation
import Combine
import SwiftUICombineToolBox
import Resolver

final class HomeViewModel: ObservableObject {
    
    @Published var homePacks: [MindfullPack] = []
    @Published var isLoading = false
    @Published var hasContentLoaded: Bool = false
    
    @Injected private var repository: MindfullPackRepositoryContract
    @Injected private var playerService: PlayerRepositoryContract
    @Injected var router: RouteToPageContract
    private var cancelBag = CancelBag()

    init() {
        setUp()
    }
    
    func fetchContent() {
        guard !isLoading, homePacks.isEmpty else {
            return
        }
        isLoading = true
        repository.packs(for: APIConfig.homeUrl)
            .share()
            .subscribe(on: RunLoop.main)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: {[weak self] completion in
                switch completion {
                case .finished:
                    self?.isLoading = false
                case .failure(let error):
                    self?.isLoading = false
                    switch error {
                    case let decodingError as DecodingError:
                        print("\(decodingError.localizedDescription)")
                    case let networkingError as NetworkingError:
                        print("\(networkingError.localizedDescription)")
                    default:
                        print("\(error.localizedDescription)")
                    }
                }
            }) { [weak self] packsContainer in
                self?.isLoading = false
                self?.homePacks = packsContainer.getPacks().filter { $0.id != "articles/support-us" }
            }.store(in: &cancelBag)
    }
}

private extension HomeViewModel {
    func setUp() {
        
        playerService.hasContentLoaded
            .receive(on: RunLoop.main)
            .assignNoRetain(to: \.hasContentLoaded, on: self)
            .store(in: &cancelBag)
    }
}
