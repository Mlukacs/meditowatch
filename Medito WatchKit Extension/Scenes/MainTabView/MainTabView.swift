//
//  MainTabView.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 06/12/2020.
//

import SwiftUI
import Resolver

struct MainTabView: View {
    @InjectedObject private var viewModel: MainTabViewModel
    @State private var playerNavigationAction: Bool = false
    @State private var navigationDeeplinkAction: Bool = false
    @State private var currentTab = 1

    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            NavigationView {
                ZStack {
                    navigationView
                    containerView
                    .navigationTitle {
                        NavigationBarLogo(iconeName: "medito")
                    }
                }
            }
            if viewModel.shouldPresentPlayerOverlay {
                playerOverlay
            }
        }.edgesIgnoringSafeArea(.bottom)
    }
}

private extension MainTabView {
    var containerView : some View {
        TabView(selection: $currentTab) {
            if viewModel.playerLoadedWithContent {
                PlayerPageView().tag(0)
            }
            HomeView().tag(1)
            UserContentHomeView().tag(2)
        }.tabViewStyle(PageTabViewStyle())
        .onAppear {
            playerNavigationAction = false
            navigationDeeplinkAction = viewModel.shouldPresentDeepLink
        }
    }
}

private extension MainTabView {
    var navigationView : some View {
        Group {
            NavigationLink(destination: LazyView(viewModel.router.routeToPlayer()), isActive: $playerNavigationAction) {
                EmptyView().frame(width: 0, height: 0)
            }.buttonStyle(PlainButtonStyle())
            NavigationLink(destination: LazyView(viewModel.router.deepLink()), isActive: $navigationDeeplinkAction) {
                EmptyView().frame(width: 0, height: 0)
            }.buttonStyle(PlainButtonStyle())
            .hidden()
        }
    }
}

private extension MainTabView {
    var playerOverlay : some View {
        Button(action: { playerNavigationAction = true }) {
            HStack {
                Spacer()
                ZStack(alignment: .center) {
                    Circle()
                        .foregroundColor(.darkBGColor)
                        .frame(width: 35, height: 35)
                    
                    Image(systemName: "playpause.fill")
                        .resizable()
                        .renderingMode(.template)
                        .scaledToFit()
                        .foregroundColor(.defaultMainCoral)
                        .frame(width: 15, height: 15)
                }
            }
        }.buttonStyle(PlainButtonStyle())
        .padding()
    }
}

struct MainTabView_Previews: PreviewProvider {
    static var previews: some View {
        MainTabView()
    }
}
