//
//  MainTabViewModel.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 02/01/2021.
//

import Foundation
import Combine
import SwiftUICombineToolBox
import Resolver

final class MainTabViewModel: ObservableObject {
    @Published var shouldPresentPlayerOverlay: Bool = false
    @Published var shouldPresentDeepLink: Bool = false
    @Published var playerLoadedWithContent: Bool = false
    
    @Injected private var playerService: PlayerRepositoryContract
    @Injected var router: RouteToPageContract
    private var cancelBag = CancelBag()

    init() {
        setUp()
    }
}

private extension MainTabViewModel {
    func setUp() {
        playerService.hasContentLoaded
            .receive(on: DispatchQueue.main)
            .assignNoRetain(to: \.playerLoadedWithContent, on: self)
            .store(in: &cancelBag)
        
        router.shouldPresentDeepLink
            .removeDuplicates()
            .receive(on: DispatchQueue.main)
            .assignNoRetain(to: \.shouldPresentDeepLink, on: self)
            .store(in: &cancelBag)
        
        Publishers.CombineLatest(router.shouldPresentPlayerOverlay, playerService.hasContentLoaded)
            .share()
            .map { shouldPresentOverlay, hascontent in
                if shouldPresentOverlay && hascontent {
                    return true
                } else {
                    return false
                }
            }
            .receive(on: DispatchQueue.main)
            .removeDuplicates()
            .assignNoRetain(to: \.shouldPresentPlayerOverlay, on: self)
            .store(in: &cancelBag)
    }
}
