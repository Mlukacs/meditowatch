//
//  OfflineView.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 02/01/2021.
//

import SwiftUI
import Resolver

struct OfflineView: View {
    @InjectedObject private var viewModel: OfflineViewModel

    var body: some View {
        containerView
            .navigationBarTitle(Text("Downloaded"))
    }
}

// MARK: Main container view
private extension OfflineView {
    var containerView : some View {
        Group {
            if viewModel.data.isEmpty {
                emptyView
            } else {
                fullOfflineView
            }
        }
    }
}

private extension OfflineView {
    var fullOfflineView : some View {
        Group {
            List {
                ForEach(viewModel.data) { offlineAudio in
                    NavigationLink(destination: LazyView(viewModel.router.routeToAudioPage(for: offlineAudio, isOffline: true))) {
                        AudioDataRow(data: offlineAudio)
                    }
                }
                .listRowInsets(EdgeInsets())
            }.listStyle(CarouselListStyle())
        }
    }
}

private extension OfflineView {
    var emptyView: some View {
        VStack {
            Text("It's a bit empty here. You should add content to your favorites.")
        }
    }
}

struct OfflineView_Previews: PreviewProvider {
    static var previews: some View {
        OfflineView()
    }
}
