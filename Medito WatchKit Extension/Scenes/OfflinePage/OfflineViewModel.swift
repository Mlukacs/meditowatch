//
//  OfflineViewModel.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 02/01/2021.
//

import Foundation
import Combine
import SwiftUICombineToolBox
import Resolver

final class OfflineViewModel: ObservableObject {

    @Published var data: [AudioData] = []
    @Published var hasContentLoaded: Bool = false
    
    @Injected private var localRepository: LocalRepositoryContract
    @Injected var router: RouteToPageContract
    private var cancelBag = CancelBag()
    
    init() {
        setUp()
    }
}

private extension OfflineViewModel {
    func setUp() {
        localRepository.localContent
            .receive(on: DispatchQueue.main)
            .assignNoRetain(to: \.data, on: self)
            .store(in: &cancelBag)
    }
}
