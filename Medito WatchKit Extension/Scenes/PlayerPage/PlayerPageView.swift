//
//  PlayerPageView.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 06/12/2020.
//

import SwiftUI
import Resolver

struct PlayerPageView: View {
    @Environment(\.presentationMode) private var presentationMode
    @InjectedObject var viewModel: PlayerPageViewModel
    @Injected var volumeView: VolumeView

    @State private var alpha: Double = 0

    var body: some View {
        containerView
            .onAppear(perform: {
                if presentationMode.wrappedValue.isPresented {
                    viewModel.startToPlay()
                }
            })
            .background(volumeView.opacity(0))
    }
    
    private func hapticFeedBack() {
        WKInterfaceDevice.current().play(.click)
    }
}

// MARK: Main container view
private extension PlayerPageView {
    var containerView : some View {
        Group {
            if viewModel.isLoading {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .white))
            } else {
                playerView
            }
        }
    }
}

private extension PlayerPageView {
    var playerView : some View {
        VStack {
            mainInfos
            HStack {
                backwardButton
                playButton
                forwardButton
            }.padding(.top, 7)
            .padding(.bottom, 10)
            HStack {
                Text(viewModel.currentTime)
                    .fontWeight(.light)
                    .font(.system(size: 11))
                    .frame(width: 35)
                ProgressView(value: viewModel.currentProgress)
                    .progressViewStyle(LinearProgressViewStyle(tint:
                                                                Color(hex: viewModel.audioData?.content.getPrimaryColor() ?? "")))
                Text(viewModel.totalDuration)
                    .fontWeight(.light)
                    .font(.system(size: 11))
                    .frame(width: 35)
            }
        }
    }
}

// MARK: Main container view
private extension PlayerPageView {
    var mainInfos : some View {
        HStack {
            Spacer().frame(width: 10)
            
            Button(action: {
                hapticFeedBack()
                viewModel.toggleBackgroundSound()
            }) {
                Image(systemName: "speaker.slash")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 15, height: 15)
                    .foregroundColor(viewModel.isBackgroundSoundPlaying ? .white : .red)
            }.buttonStyle(PlainButtonStyle())
            .opacity(viewModel.selectedSound != nil ? 1 : 0)
            
            Spacer()
            Text(viewModel.audioData?.title ?? "")
                .fontWeight(.semibold)
                .font(.system(size: 13))
                .lineLimit(3)
                .multilineTextAlignment(.center)
                .minimumScaleFactor(0.9)
                .padding(2)

            Spacer()
            ZStack {
                Image("sound_volume")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 20, height: 15)
                VolumeCircularProgressBar(progress: viewModel.volume)
                   
            }.frame(width: 20, height: 20)
            .onReceive(viewModel.$volume) { _ in
                self.alpha = 1
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.alpha = 0
                }
            }
            .opacity(alpha)
            Spacer().frame(width: 10)
        }
    }
}

private extension PlayerPageView {
    var playButton : some View {
        Button(action: {
            hapticFeedBack()
            viewModel.togglePlay()
        }) {
            HStack {
                Spacer()
                ZStack(alignment: .center) {
                    Image("blurb")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundColor(Color(hex: viewModel.audioData?.content.getPrimaryColor() ?? ""))
                        .frame(width: 50, height: 50)
                    Image(systemName: viewModel.isPlaying ? "pause.fill" : "play.fill")
                        .resizable()
                        .renderingMode(.template)
                        .foregroundColor(.black)
                        .frame(width: 15, height: 15)
                }
                Spacer()
            }
        }.buttonStyle(PlainButtonStyle())
        .padding()
    }
}

private extension PlayerPageView {
    var forwardButton : some View {
        Button(action: {
            hapticFeedBack()
            viewModel.seek(by: 15, and: .forward)
        }) {
            Image(systemName: "goforward.15")
                .resizable()
                .renderingMode(.template)
                .frame(width: 20, height: 20)
            
        }.buttonStyle(PlainButtonStyle())
        .padding()
    }
}

private extension PlayerPageView {
    var backwardButton : some View {
        Button(action: {
            hapticFeedBack()
            viewModel.seek(by: 15, and: .backward)
        }) {
            Image(systemName: "gobackward.15")
                .resizable()
                .renderingMode(.template)
                .frame(width: 20, height: 20)
        }.buttonStyle(PlainButtonStyle())
        .padding()
    }
}

// MARK: - UIelement for player
struct VolumeView: WKInterfaceObjectRepresentable {
    typealias WKInterfaceObjectType = WKInterfaceVolumeControl
    var origin: WKInterfaceVolumeControl.Origin = .local
    
    func makeWKInterfaceObject(context: Self.Context) -> WKInterfaceVolumeControl {
        let view = WKInterfaceVolumeControl(origin: origin)
        view.focus()
        return view
    }
    
    func updateWKInterfaceObject(_ wkInterfaceObject: WKInterfaceVolumeControl,
                                 context: WKInterfaceObjectRepresentableContext<VolumeView>) {}
}

private struct VolumeCircularProgressBar: View {
    var progress: Float
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(lineWidth: 1.0)
                .opacity(0.3)
                .foregroundColor(.white)
            
            Circle()
                .trim(from: 0.0, to: CGFloat(min(self.progress, 1.0)))
                .stroke(style: StrokeStyle(lineWidth: 1.0, lineCap: .round, lineJoin: .round))
                .foregroundColor(.green)
                .rotationEffect(Angle(degrees: 90.0))
        }
    }
}

struct PlayerPageView_Previews: PreviewProvider {
    static var previews: some View {
        PlayerPageView()
    }
}
