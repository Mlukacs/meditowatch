//
//  PlayerPageViewModel.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 06/12/2020.
//

import Foundation
import AVFoundation
import WatchKit
import Combine
import SwiftUICombineToolBox
import Resolver

enum SeekDirection {
    case forward
    case backward
}

final class PlayerPageViewModel: ObservableObject {
    @Published var audioData: AudioData?
    @Published var selectedFile: File?
    @Published var selectedSound: BackgroundSound?
    
    @Published var currentTime = "00.00"
    @Published var currentProgress: Float = 0.0
    @Published var totalDuration = "00.00"
    @Published var volume: Float = 0.0
    @Published var isPlaying = false
    @Published var isBackgroundSoundPlaying = false
    @Published var isLoading = false

    @Injected private var playerService: PlayerRepositoryContract
    private var cancelBag = CancelBag()
    
    init() {
        setUp()
    }

    func seek(by time: Double, and direction: SeekDirection) {
        playerService.seek(by: time, and: direction)
    }
    
    func setUpViewModel(with data: AudioData?,
                        audioFile: File?,
                        backgroundSound: BackgroundSound?) {
        guard !playerService.dataAlreadyLoaded(with: data,
                                               audioFile: audioFile,
                                               backgroundSound: backgroundSound) else {
            return
        }
        playerService.loadPlayers(with: data, audioFile: audioFile, backgroundSound: backgroundSound)
    }
    
    func togglePlay() {
        #if targetEnvironment(simulator)
        playerService.togglePlaying()
        #else
        AVAudioSession.sharedInstance().activate(options: []) { [weak self] active, error in
            if error != nil {
                return
            } else if active {
                DispatchQueue.main.async { [weak self] in
                    self?.playerService.togglePlaying()
                }
            }
        }
        #endif
    }
    
    func toggleBackgroundSound() {
        playerService.toggleBackgroundSoundPlaying()
    }

    func startToPlay() {
        guard playerService.playerCurrentStatus.value != .playing else {
            return
        }
        #if targetEnvironment(simulator)
        playerService.startPlaying()
        #else
        AVAudioSession.sharedInstance().activate(options: []) { [weak self] active, error in
            if error != nil {
                return
            } else if active {
                DispatchQueue.main.async { [weak self] in
                    self?.playerService.startPlaying()
                }
            }
        }
        #endif
   }
}

private extension PlayerPageViewModel {
    func setUp() {
        playerService.isCurrentlyLoading
            .receive(on: RunLoop.main)
            .assignNoRetain(to: \.isLoading, on: self)
            .store(in: &cancelBag)
        
        playerService.currentProgressPublisher
            .receive(on: RunLoop.main)
            .assignNoRetain(to: \.currentProgress, on: self)
            .store(in: &cancelBag)
        
        playerService.currentTimePublisher
            .receive(on: RunLoop.main)
            .assignNoRetain(to: \.currentTime, on: self)
            .store(in: &cancelBag)
        
        playerService.currentTotalDuration
            .receive(on: RunLoop.main)
            .assignNoRetain(to: \.totalDuration, on: self)
            .store(in: &cancelBag)

        AVAudioSession.sharedInstance().publisher(for: \.outputVolume)
            .receive(on: DispatchQueue.main)
            .assignNoRetain(to: \.volume, on: self)
            .store(in: &cancelBag)
        
        playerService.playerCurrentStatus
            .receive(on: RunLoop.main)
            .map { value in
                value == .playing
            }
            .assignNoRetain(to: \.isPlaying, on: self)
            .store(in: &cancelBag)
        
        playerService.currentAudioDataPublisher
            .receive(on: DispatchQueue.main)
            .assignNoRetain(to: \.audioData, on: self)
            .store(in: &cancelBag)
        
        playerService.currentFilePublisher
            .receive(on: RunLoop.main)
            .assignNoRetain(to: \.selectedFile, on: self)
            .store(in: &cancelBag)
        
        playerService.currentBackgroundSoundPublisher
            .receive(on: RunLoop.main)
            .assignNoRetain(to: \.selectedSound, on: self)
            .store(in: &cancelBag)
        
        playerService.isBackgroundSoundPlaying
            .receive(on: RunLoop.main)
            .assignNoRetain(to: \.isBackgroundSoundPlaying, on: self)
            .store(in: &cancelBag)
    }
}
