//
//  ReminderCreationView.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 23/01/2021.
//

import SwiftUI
import Resolver

struct ReminderCreationView: View {
    @Environment(\.presentationMode) private var presentationMode
    @InjectedObject private var viewModel: ReminderCreationModel
    
    var body: some View {
        containerView.navigationBarTitle("Daily Alarms")
    }
}

// MARK: Main container view
private extension ReminderCreationView {
    var containerView : some View {
        Form {
            typeSectionView
            timeSectionView
            addButtonView
            cancelButtonView
        }
    }
}

// MARK: Main container view
private extension ReminderCreationView {
    var typeSectionView : some View {
        Section(header: Text("Type of Session")) {
            Picker(selection: $viewModel.selectedType, label: Text("Mindfull Type")) {
                ForEach(0 ..< viewModel.types.count) {
                    Text(viewModel.types[$0].rawValue)
                }
            }
            .labelsHidden()
            .frame(height: 40)
            .clipped()
            .padding(.horizontal, 10)
        }
        
    }
}

// MARK: Main container view
private extension ReminderCreationView {
    var timeSectionView : some View {
        Section(header: Text("Time for the reminder")) {
            HStack(alignment: .center) {
                Picker(selection: $viewModel.selectedHours, label: Text("")) {
                    ForEach(0 ..< viewModel.hours.count) {
                        Text("\(viewModel.hours[$0])")
                    }
                }.pickerStyle(WheelPickerStyle())
                .labelsHidden()
                .frame(height: 40)
                .clipped()
                .padding(.horizontal, 10)
                Text(":")
                Picker(selection: $viewModel.selectedMinutes, label: Text("")) {
                    ForEach(0 ..< viewModel.minutes.count) {
                        Text("\(viewModel.minutes[$0])")
                    }
                }.pickerStyle(WheelPickerStyle())
                .labelsHidden()
                .frame(height: 40)
                .clipped()
                .padding(.horizontal, 10)
            }
        }
    }
}

// MARK: Main container view
private extension ReminderCreationView {
    var addButtonView : some View {
        Button(action: {
            viewModel.create()
            self.presentationMode.wrappedValue.dismiss()
        }) {
            HStack {
                Spacer()
                Text("Add")
                    .cornerRadius(10)
                Spacer()
            }
        }.buttonStyle(PlainButtonStyle())
    }
}

private extension ReminderCreationView {
    var cancelButtonView: some View {
        Button(action: {
            self.presentationMode.wrappedValue.dismiss()
        }) {
            HStack {
                Spacer()
                Text("Cancel")
                    .cornerRadius(10)
                Spacer()
            }
        }.buttonStyle(PlainButtonStyle())
    }
}

struct ReminderCreationView_Previews: PreviewProvider {
    static var previews: some View {
        ReminderCreationView()
    }
}
