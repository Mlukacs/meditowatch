//
//  ReminderCreationViewModel;.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 24/01/2021.
//

import Foundation
import Combine
import SwiftUICombineToolBox
import Resolver

final class ReminderCreationModel: ObservableObject {
    @Published  var selectedType = 0
    @Published  var selectedHours = 12
    @Published  var selectedMinutes = 30
    let types = ReminderType.allCases.map { $0 }
    var hours = Array(0...23)
    var minutes = Array(0...59)
    
    @Injected private var userNotificationRepo: UserNotificationRepositoryContract
    private var cancelBag = CancelBag()
    
    init() {
        setUp()
    }
    
    func create() {
        userNotificationRepo.scheduleNotification(for: types[selectedType],
                                                  hour: hours[selectedHours],
                                                  minutes: minutes[selectedMinutes])
    }
}

private extension ReminderCreationModel {
    func setUp() {}
}
