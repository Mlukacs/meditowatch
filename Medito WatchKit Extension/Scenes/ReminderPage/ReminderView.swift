//
//  ReminderView.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 21/01/2021.
//

import SwiftUI
import Resolver

struct ReminderView: View {
    
    @InjectedObject private var viewModel: ReminderViewModel
    @State private var seconds: TimeInterval = 60 * 60 * 12
    @State var showingDetail = false
    
    static let formatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute]
        return formatter
    }()
    
    var body: some View {
        containerView
        .sheet(isPresented: $showingDetail) {
            ReminderCreationView()
        }.onAppear(perform: {
            viewModel.fetchReminders()
        })
    }
    
    private func delete(at offsets: IndexSet) {
        viewModel.remove(at: offsets)
    }
}

// MARK: Main container view
private extension ReminderView {
    var containerView : some View {
        VStack {
            topButtonViewContainer
            listViewContainer
        }
    }
}

// MARK: Main container view
private extension ReminderView {
    var topButtonViewContainer : some View {
        HStack {
            Button(action: {
                self.showingDetail.toggle()
            }) {
                Image(systemName: "calendar.badge.plus")
                    .resizable()
                    .frame(width: 25, height: 25, alignment: .center)
            }.buttonStyle(PlainButtonStyle())
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 40, idealHeight: 40, maxHeight: 40)
            .background(Color.black.brightness(0.1))
            .cornerRadius(10, corners: [.topLeft, .bottomLeft])
            .cornerRadius(5, corners: [.topRight, .bottomRight])
            
            Button(action: {
                viewModel.clearAll()
            }) {
                Image(systemName: "trash")
                    .resizable()
                    .frame(width: 25, height: 25, alignment: .center)
                    .foregroundColor(.white)
            }.buttonStyle(PlainButtonStyle())
            .frame(minWidth: 0, maxWidth: .infinity, minHeight: 40, idealHeight: 40, maxHeight: 40)
            .background(Color.black.brightness(0.1))
            .cornerRadius(10, corners: [.topRight, .bottomRight])
            .cornerRadius(5, corners: [.topLeft, .bottomLeft])
        }
    }
}

// MARK: Main container view
private extension ReminderView {
    var listViewContainer : some View {
        List {
            ForEach(viewModel.reminders, id: \.identifier) { reminder in
                HStack {
                    Image(systemName: viewModel.imageName(for: reminder))
                    Spacer()
                    VStack {
                        Text(viewModel.name(for: reminder))
                            .fontWeight(.semibold)
                        Text(viewModel.time(for: reminder))
                            .fontWeight(.light)
                            .font(.system(size: 13))
                            .foregroundColor(.gray)
                    }
                    Spacer()
                }
            }
            .onDelete(perform: delete )
        }.listStyle(CarouselListStyle())
    }
}

struct ReminderView_Previews: PreviewProvider {
    static var previews: some View {
        ReminderView()
    }
}

struct DetailView: View {
    var body: some View {
        Text("Detail")
    }
}
