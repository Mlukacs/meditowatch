//
//  ReminderViewModel.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 21/01/2021.
//

import Foundation
import UserNotifications
import Combine
import SwiftUICombineToolBox
import Resolver

final class ReminderViewModel: ObservableObject {
    
    @Published var reminders: [UNNotificationRequest] = []
    @Published var autorizationGranted = false
    @Published var currentDate = Date()
    @Injected private var userNotificationRepo: UserNotificationRepositoryContract
    
    let center = UNUserNotificationCenter.current()
    
    private var cancelBag = CancelBag()
    
    init() {
        setUp()
    }
    
    func fetchReminders() {
        userNotificationRepo.fetchAllActiveReminder()
            .receive(on: DispatchQueue.main)
            .map { reminders in
                let sortedReminders = reminders.sorted {
                    guard let trigger1 = $0.trigger as? UNCalendarNotificationTrigger,
                          let trigger2 = $1.trigger as? UNCalendarNotificationTrigger,
                          let hours1 = trigger1.dateComponents.hour,
                          let minutes1 = trigger1.dateComponents.minute,
                    let hours2 = trigger2.dateComponents.hour,
                        let minutes2 = trigger2.dateComponents.minute
                    else {
                        return false
                    }
                    if hours1 == hours2 {
                        return minutes1 < minutes2
                    } else {
                        return hours1 < hours2
                    }
                }
                return sortedReminders
            }
            .assignNoRetain(to: \.reminders, on: self)
            .store(in: &cancelBag)
    }
    
    func clearAll() {
        userNotificationRepo.clearAll()
        fetchReminders()
    }
    
    func remove(at offset: IndexSet) {
        userNotificationRepo.removeRemidner(with: reminders[offset.first ?? 0].identifier)
        fetchReminders()
    }
    
    func imageName(for reminder: UNNotificationRequest) -> String {
        
        guard let customData = reminder.content.userInfo["MeditoReminderType"] as? String else {
            return "clock"
        }
        switch customData {
        case ReminderType.daily.rawValue:
            return "clock"
        case ReminderType.sound.rawValue:
            return "music.note"
        case ReminderType.sleep.rawValue:
            return "powersleep"
        case ReminderType.timer.rawValue:
            return "timer"
        default:
            return "clock"
        }
    }
    
    func name(for reminder: UNNotificationRequest) -> String {
        guard let customData = reminder.content.userInfo["MeditoReminderType"] as? String else {
            return "Daily"
        }
        return customData
    }
    
    func time(for reminder: UNNotificationRequest) -> String {
        guard let trigger = reminder.trigger as? UNCalendarNotificationTrigger,
              let hours = trigger.dateComponents.hour,
              let minutes = trigger.dateComponents.minute else {
            return "00:00"
        }
        let stringHours = hours < 10 ? "0\(hours)" : "\(hours)"
        let stringMinutes = minutes < 10 ? "0\(minutes)": "\(minutes)"
        return "\(stringHours):\(stringMinutes)"
    }
}

private extension ReminderViewModel {
    func setUp() {
        userNotificationRepo.askPermission()
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { [weak self] completion in
                switch completion {
                case .finished:
                    break
                case .failure:
                    self?.autorizationGranted = false
                }
                
            }, receiveValue: { [weak self] permission in
                self?.autorizationGranted = permission
            }).store(in: &cancelBag)
    }
}
