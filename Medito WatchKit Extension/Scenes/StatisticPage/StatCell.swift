//
//  StatCell.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 20/01/2021.
//

import SwiftUI

struct StatCell: View {
    var title: String
    var description: String
    
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            Text("\(title)")
                .multilineTextAlignment(.leading)
            Text(description)
                .font(.system(size: 25))
                .bold()
        }.padding(10)
        .frame(maxWidth: .infinity, alignment: .topLeading)
        .background(Color.black.brightness(0.1))
        .cornerRadius(10)
    }
}

struct StatCell_Previews: PreviewProvider {
    static var previews: some View {
        StatCell(title: "Average time per meditation session", description: "12 minutes ")
    }
}
