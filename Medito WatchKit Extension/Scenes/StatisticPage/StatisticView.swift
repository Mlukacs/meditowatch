//
//  StatisticPageView.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 19/01/2021.
//

import SwiftUI
import Resolver

struct StatisticView: View {
    
    @InjectedObject private var viewModel: StatisticViewModel
    
    var body: some View {
        containerView.navigationBarTitle(Text("Stats"))
    }
}

// MARK: Main container view
private extension StatisticView {
    var containerView : some View {
        VStack {
            pickerViewContainer
            scrollviewViewContainer
        }
    }
}

private extension StatisticView {
    var pickerViewContainer : some View {
        Picker(selection: $viewModel.pickerSelection, label: Text("Stats")) {
            Text("Week").tag(0)
            Text("Month").tag(1)
            Text("Year").tag(2)
        }
        .labelsHidden()
        .frame(height: 40)
        .clipped()
        .padding(.horizontal, 10)
    }
}

private extension StatisticView {
    var scrollviewViewContainer : some View {
        ScrollView {
            VStack(alignment: .center, spacing: 5) {
                StatCell(title: "Total number of minutes", description: "\(viewModel.numberOfMinutes) minutes")
                StatCell(title: "Number of mindfull Sessions", description: "\(viewModel.numberOfSession) sessions")
                StatCell(title: "Average minutes per session", description: "\(viewModel.averageTimeBySession)")
            }
        }
    }
}

struct StatisticView_Previews: PreviewProvider {
    static var previews: some View {
        StatisticView()
    }
}
