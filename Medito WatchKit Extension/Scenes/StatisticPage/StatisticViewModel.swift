//
//  StatisticViewModel.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 19/01/2021.
//

import Foundation
import Combine
import SwiftUICombineToolBox
import Resolver

final class StatisticViewModel: ObservableObject {

    @Published var pickerSelection = 0
    @Published var numberOfMinutes = 0
    @Published var numberOfSession = 0
    @Published var averageTimeBySession: Double = 0

    @Injected private var healthKit: HealthKitServiceContract
    private var cancelBag = CancelBag()
    
    init() {
        setUp()
    }
}

private extension StatisticViewModel {
    func setUp() {
        healthKit.healthKitData
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] data in
                self?.numberOfMinutes = data.totalMinutes
                self?.numberOfSession = data.numberOfEvents
                self?.averageTimeBySession = data.averageTimeSession.roundToPlaces(2)
            })
            .store(in: &cancelBag)
        
        $pickerSelection
            .removeDuplicates()
            .receive(on: DispatchQueue.main)
            .sink(receiveValue: { [weak self] amoutOfTimeIndex in
                var amountOfTime: HealtKitTimeSpand = .week
                switch amoutOfTimeIndex {
                case 1:
                    amountOfTime = .month
                case 2:
                    amountOfTime = .year
                default:
                    amountOfTime = .week
                }
                self?.healthKit.getMindfullData(forPast: amountOfTime)
            })
            .store(in: &cancelBag)
    }
}
