//
//  TextPageView.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 12/12/2020.
//

import SwiftUI
import Resolver

struct TextPageView: View {
    @InjectedObject var viewModel: TextPageViewModel

    var body: some View {
        containerView
            .navigationBarTitle(Text(viewModel.getTitle()))
            .onAppear(perform: { viewModel.fetchContent() })
    }
}

// MARK: Main container view
private extension TextPageView {
    var containerView : some View {
        Group {
            if viewModel.isLoading {
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle(tint: .white))
            } else if viewModel.text == nil {
                emptyView
            } else {
                ScrollView {
                    Text(viewModel.getContent())
                        .padding(5)
                }
            }
        }
    }
}

private extension TextPageView {
    var emptyView : some View {
        VStack {
            Text("No Content to display. Try and refresh the page.")
            Button(action: {
                self.viewModel.fetchContent()
            }) {
                Spacer()
                Text("Refresh")
                    .padding()
                Spacer()
            }.buttonStyle(PlainButtonStyle())
            .frame(height: 45)
            .background(Color.defaultMainCoral)
            .cornerRadius(15)
        }
    }
}

struct TextPageView_Previews: PreviewProvider {
    static var previews: some View {
        TextPageView()
    }
}
