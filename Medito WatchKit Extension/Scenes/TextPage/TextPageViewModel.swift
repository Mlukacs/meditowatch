//
//  TextPageViewModel.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 12/12/2020.
//

import Foundation
import Combine
import SwiftUICombineToolBox
import Resolver

final class TextPageViewModel: ObservableObject {

    @Published var text: TextData?
    @Published var hasContentLoaded: Bool = false
    @Published var isLoading = false

    @Injected private var textRepository: TextDataRepositoryContract
    @Injected private var playerService: PlayerRepositoryContract
    @Injected var router: RouteToPageContract
    private var path: String?
    private var cancelBag = CancelBag()
    
    init() {
        setUp()
    }
    
    func setUpViewModel(with contentPath: String) {
        path = contentPath
    }
    
    func getTitle() -> String {
        text?.getTitle() ?? ""
    }
    
    func getContent() -> String {
        text?.getBody() ?? ""
    }
    
    func fetchContent() {
        guard let path = path else {
            return
        }
        isLoading = true
        textRepository.textData(for: path)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: {[weak self] completion in
                switch completion {
                case .finished:
                    self?.isLoading = false
                case .failure(let error):
                    self?.isLoading = false
                    switch error {
                    case let decodingError as DecodingError:
                        print("\(decodingError.localizedDescription)")
                    case let networkingError as NetworkingError:
                        print("\(networkingError.localizedDescription)")
                    default:
                        print("\(error.localizedDescription)")
                    }
                }
            }) { [weak self] textData in
                guard let self = self else {
                    return
                }
                self.isLoading = false
                self.text = textData
            }.store(in: &cancelBag)
    }
}

private extension TextPageViewModel {
    func setUp() {
        playerService.hasContentLoaded
            .receive(on: RunLoop.main)
            .assignNoRetain(to: \.hasContentLoaded, on: self)
            .store(in: &cancelBag)
    }
}
