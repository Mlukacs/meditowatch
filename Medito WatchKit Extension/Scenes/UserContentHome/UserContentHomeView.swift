//
//  UserContentHome.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 06/12/2020.
//

import SwiftUI
import Resolver

struct UserContentHomeView: View {
    @InjectedObject private var viewModel: UserContentHomeViewModel
    
    var body: some View {
        containerView.navigationTitle {
            Text( "Your content" )
        }
    }
}

// MARK: Main container view
private extension UserContentHomeView {
    var containerView : some View {
        fullTilesView
    }
}

private extension UserContentHomeView {
    var fullTilesView : some View {
        Group {
            List {
                ForEach(viewModel.userHomeTiles) { tile in
                    NavigationLink(destination:
                                    LazyView(viewModel.router.routeToPage(for: tile))
                    ) {
                        UserTileRow(tile: tile)
                    }
                }
                .listRowInsets(EdgeInsets())
            }.listStyle(CarouselListStyle())
        }
    }
}

// MARK: - Page UI elements

struct UserTileRow: View {
    var tile: UserContentTile
    
    var body: some View {
        base
            .cornerRadius(5.0)
    }
}

private extension UserTileRow {
    var base : some View {
        HStack {
            coverImage
            Spacer()
            infoContainer
            Spacer()
        }
    }
}

// MARK: - Cover Image
private extension UserTileRow {
    var coverImage : some View {
        Image(systemName: tile.imageName)
            .resizable()
            .renderingMode(.template)
            .foregroundColor(tile.color)
            .frame(width: 20, height: 20)
            .aspectRatio(contentMode: .fit)
            .padding(.leading, 10)
    }
}

// MARK: - Playlist information display
private extension UserTileRow {
    var infoContainer: some View {
        VStack(alignment: .center) {
            HStack {
                Text(tile.title)
                    .fontWeight(.semibold)
                    .font(.system(size: 14))
                    .lineLimit(2)
                    .foregroundColor(.white)
                    .multilineTextAlignment(.center)
            }
        }
    }
}

struct UserContentHomeView_Previews: PreviewProvider {
    static var previews: some View {
        UserContentHomeView()
    }
}
