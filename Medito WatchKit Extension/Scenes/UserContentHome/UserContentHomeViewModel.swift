//
//  UserContentHomeViewModel.swift
//  Medito WatchKit Extension
//
//  Created by Martin Lukacs on 06/12/2020.
//

import Foundation
import Combine
import SwiftUICombineToolBox
import Resolver

final class UserContentHomeViewModel: ObservableObject {
    
    @Published var userHomeTiles: [UserContentTile] = []
    @Published var hasContentLoaded: Bool = false

    @Injected var router: RouteToUserPageContract
    @Injected private var playerService: PlayerRepositoryContract
    private var cancelBag = CancelBag()

    init() {
        setUp()
    }
}

private extension UserContentHomeViewModel {
    func setUp() {
        let favoriteTile = UserContentTile(id: UUID(),
                                           title: "Favorites",
                                           imageName: "heart.fill",
                                           type: .favorites,
                                           color: .red)
        let offlineTile = UserContentTile(id: UUID(),
                                           title: "Downloads",
                                           imageName: "square.and.arrow.down",
                                           type: .offline,
                                           color: .white)
        let statisticTile = UserContentTile(id: UUID(),
                                            title: "Stats",
                                            imageName: "chart.bar.xaxis",
                                            type: .dataTracking,
                                            color: .white)
        let reminderTile = UserContentTile(id: UUID(),
                                           title: "Reminder",
                                           imageName: "calendar.badge.clock",
                                           type: .reminder,
                                           color: .white)
        userHomeTiles.append(favoriteTile)
        userHomeTiles.append(offlineTile)
        userHomeTiles.append(statisticTile)
        userHomeTiles.append(reminderTile)

        playerService.hasContentLoaded
            .receive(on: RunLoop.main)
            .assignNoRetain(to: \.hasContentLoaded, on: self)
            .store(in: &cancelBag)
    }
}
