Welcome to the Medito apple watch project

To test this app you will need to compile it with xcode.
After the first compilation a Medito-info.plist file will appear in the configuration folder.
You will need to set a valid API key in this file to be able to fetch content from the back end and make the app work.

Hope you have fun with it.
